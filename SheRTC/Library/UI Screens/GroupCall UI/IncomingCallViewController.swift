//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation


class DSIncomingCallViewController: UIViewController {
    let roomEntity: DSRoomEntity
    
    var blurView: UIVisualEffectView!
    var dstarImageView: UIImageView!
    
    var contactNameLabel: UILabel!
    
    var incomingCallControls: UIStackView!
    var acceptIncomingButton: UIButton!
    var declineIncomingButton: UIButton!
    var vibrateGenerator: UINotificationFeedbackGenerator!
    
    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = UIColor.darkGray
        self.view.layoutMargins = UIEdgeInsets(top: 16, left: 20, bottom: 16, right: 20)
        createViews()
        layoutViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactNameLabel.text = roomEntity.displayNameForGroupCall
        vibrateGenerator = UINotificationFeedbackGenerator()
        vibrate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) { [weak self] in
            guard let self = self else { return }
            self.didPressDeclineCall(UIButton())
        }
    }
    
    init(room: DSRoomEntity) {
        self.roomEntity = room
        super.init(nibName: nil, bundle: nil)
        
        let unlinkNotifyName = Notification.Name(Notification_UnlinkVideoconferencing)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(unlinkCommand(_:)),
                                               name: unlinkNotifyName,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createViews() {
        view.isUserInteractionEnabled = true
        dstarImageView = UIImageView()
        dstarImageView.image = #imageLiteral(resourceName: "logoSignal.png")
        view.addSubview(dstarImageView)
        // Dark blurred background.
        let blurEffect = UIBlurEffect(style: .dark)
        blurView = UIVisualEffectView(effect: blurEffect)
        blurView.isUserInteractionEnabled = false
        view.addSubview(blurView)
        view.bringSubviewToFront(blurView)
        
        // Create the video views first, as they are under the other views.
        createContactViews()
        createIncomingCallControls()
    }
    func createContactViews() {
        contactNameLabel = UILabel()
        
        contactNameLabel.textAlignment = .center
        contactNameLabel.textColor = UIColor.white
        contactNameLabel.layer.shadowOffset = CGSize.zero
        contactNameLabel.layer.shadowOpacity = 0.35
        contactNameLabel.layer.shadowRadius = 4

        view.addSubview(contactNameLabel)
        contactNameLabel.configureForAutoLayout()
        contactNameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 40)
        contactNameLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 40)
    }
    func createIncomingCallControls() {

        acceptIncomingButton = createButton(image: #imageLiteral(resourceName: "call-active-wide"),
                                            action: #selector(didPressAnswerCall))
        acceptIncomingButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_ACCEPT_INCOMING_CALL_LABEL",
                                                                    comment: "Accessibility label for accepting incoming calls")
        declineIncomingButton = createButton(image: #imageLiteral(resourceName: "hangup-active-wide"),
                                             action: #selector(didPressDeclineCall))
        declineIncomingButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_DECLINE_INCOMING_CALL_LABEL",
                                                                     comment: "Accessibility label for declining incoming calls")

        incomingCallControls = UIStackView(arrangedSubviews: [acceptIncomingButton, declineIncomingButton])
        incomingCallControls.axis = .horizontal
        incomingCallControls.alignment = .center
        incomingCallControls.distribution = .equalSpacing

        view.addSubview(incomingCallControls)
        incomingCallControls.configureForAutoLayout()
        incomingCallControls.autoPinEdge(toSuperviewEdge: .bottom, withInset: 40 )
        incomingCallControls.autoPinEdge(toSuperviewEdge: .trailing, withInset: 40)

    }
    func layoutViews() {
        dstarImageView.configureForAutoLayout()
        dstarImageView.autoCenterInSuperview()
    }
    
    @objc func didPressAnswerCall(_ button: UIButton) {
        vibrateGenerator.notificationOccurred(.success)
        
        guard let callUIAdapter = AppEnvironment.shared.conferenceCallService.callUIAdapter else {
            owsFailDebug("missing group callUIAdapter")
            return
        }
        callUIAdapter.startAndShowOutgoingCall(roomEntity: roomEntity)
        // May it cause strange behavior?
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func didPressDeclineCall(_ button: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func unlinkCommand(_ notification: Notification) {
         self.dismiss(animated: false, completion: nil)
    }
}

extension DSIncomingCallViewController {
    func createButton(image: UIImage, action: Selector) -> UIButton {
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: buttonInset(),
                                              left: buttonInset(),
                                              bottom: buttonInset(),
                                              right: buttonInset())
        button.addTarget(self, action: action, for: .touchUpInside)
        button.autoSetDimension(.width, toSize: buttonSize())
        button.autoSetDimension(.height, toSize: buttonSize())
        return button
    }
    
    
    func buttonSize() -> CGFloat {
        return 90
    }
    
    func buttonInset() -> CGFloat {
        return 8
    }
    
    // Only for debug purpose
    func vibrate() {
        DispatchQueue.global().async {
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { [weak self] in
                guard let self = self else { return }
                self.vibrate()
            }
        }
    }

}
