//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import UIKit
//import PureLayout

let cellId = "cell"
let kViewDontHideTag = 111

@objc
public class DSGroupCallViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, DSGroupCallLocalVideoViewDelegate {
    
    var callUIAdapter: DSCallUIAdapter {
        return AppEnvironment.shared.conferenceCallService.callUIAdapter
    }
    var avAudioSession: AVAudioSession {
        return AVAudioSession.sharedInstance()
    }
    
    private var collectionView: UICollectionView = {
        let layout = DSConferenceViewLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.register(DSGroupCallCollectionCell.self, forCellWithReuseIdentifier: cellId)
        collection.isUserInteractionEnabled = false
        return collection
    }()
    // MARK: - RTC -
    var roomEntity: DSRoomEntity
    
    private var rendererOfNumber = [String: RTCMediaStream]() {
        didSet {
            updateActiveRemoteNumbers()
            checkIfLocalVideoOnFull()
            updateMutedAvatarViews()
            Logger.debug("rendererOfNumbers set")
        }
    }
    var activeRemoteNumbers = [String]() { // Numbers with active video stream
        didSet {
            collectionView.reloadData()
            Logger.debug("activeRemoteNumbers set \(self.activeRemoteNumbers.count), reloadData()")
        }
    }
    // MARK: - Vars -
    private var isFirstParticipant = true
    private var isMainButtonViewHidden = false {
        didSet {
            updateHideButtonsTimer(buttonsHidden: self.isMainButtonViewHidden)
        }
    }
    
    private var isVideoOn = true {
        didSet {
            isVideoOn ? localVideo.startCapture() : localVideo.stopCapture()
            roomEntity.call.hasLocalVideo = isVideoOn
            checkIfLocalVideoOnFull()
            localVideo.isHidden = !self.isVideoOn
            localAvatarView.isHidden = self.isVideoOn
        }
    }
    
    private var isLocalVideoOnFull = false {
        didSet {
            if self.isLocalVideoOnFull != oldValue  {
                switchBetweenLocalVideos()
            }
        }
    }
    
    private var currentOutputDeviceType: DSVideoCallAudioSourceType = .speaker {
        willSet {
            if self.currentOutputDeviceType != newValue {
                DispatchQueue.main.async {
                    self.updateAudioSourceButton()
                }
            }
        }
    }
    //MARK: - Audio -
    var isSpeakerPhoneEnabled: Bool = false
    private var allAudioSources: Set<AudioSource> = Set()
    // MARK: - Capturers -
    private var localVideo: DSGroupCallLocalVideoView!
    private var fileCaptureController: DSNBMFileCaptureController? = nil
    
    private var localVideoConstraintsArray = [NSLayoutConstraint]()
    private var fullLocalVideoConstraintsArray = [NSLayoutConstraint]()
    // MARK:  - Views -
    private var mainButtonsView: UIView!
    private var ongoingCallControls: UIStackView!
    private var ongoingButtonsLabelStack: UIStackView!
    private var mutedAvatarViewStack: UIStackView!
    private var localAvatarView: UIView!
    // MARK: - Buttons & Labels -
    private var mainGradient: CAGradientLayer!
    
    private var titleLabel: UILabel!
    
//    private var backButton: UIButton!
    private var hangUpButton: UIButton!
    
    private var audioModeVideoButton: UIButton!
    private var muteSoundButton: UIButton!
    private var showVideoButton: UIButton!
    private var flipCameraButton: DSVideoCallButton!
    
    private var audioSourceButton: DSVideoCallAudioSourceButton!
    private var audioModeMuteButton: UIButton!
    // MARK: - Timers -
    private var statusLabel: UILabel!
    private var callDurationTimer: Timer?
    var dateFormatter: DateFormatter?
    
    private var hideMainButtonsViewTimer: Timer?
    
    // MARK: - VC Methods -
    @objc
    init(room: DSRoomEntity) {
        self.roomEntity = room
        // Change state to "idle" when call is active
        // to prevent sending busy command on hangup
        self.roomEntity.call.state = .idle
        super.init(nibName: nil, bundle: nil)
        self.allAudioSources = Set(callUIAdapter.audioService.availableInputs)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func loadView() {
        self.view = UIView()
        self.view.layoutMargins = UIEdgeInsets(top: 16, left: 20, bottom: 16, right: 20)
        view.addSubview(collectionView)
        createViews()
        createViewsConstraints()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        Logger.debug("Group call view did load")
        titleLabel.text = roomEntity.displayNameForGroupCall
        
        // Prevent device from sleeping while group call.
//        DeviceSleepManager.sharedInstance.addBlock(blockObject: self)
        AppEnvironment.shared.conferenceCallService.delegate = self
        
        if callUIAdapter.audioService.delegate != nil {
            callUIAdapter.audioService.delegate = nil
        }
        
        callUIAdapter.audioService.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(audioRouteChangeListener(notification:)), name: AVAudioSession.routeChangeNotification, object:nil)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        layoutSublayers()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        DeviceSleepManager.sharedInstance.removeBlock(blockObject:self)
        
        AppEnvironment.shared.conferenceCallService.delegate = nil
        NotificationCenter.default.removeObserver(self)
        callUIAdapter.audioService.delegate = nil
        
        Logger.debug("DSConferenceView will disappear")
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        callDurationTimer?.invalidate()
        callDurationTimer = nil
        
        invalidateHideButtonsTimerIfNeeded()
        Logger.debug("DSConferenceView did disappear")
    }
    
    deinit {
        Logger.debug("Deinit** GroupCallVC") // FIXME: sometimes not called
    }
    
    private func createViews() {
        createMainButtonsView()
        createLocalView()
        createMainGradients()
        createOngoingCallLabels()
        createOngoingCallControls()
        createAvatars()
        createGestureRecognizers()
        updateHideButtonsTimer(buttonsHidden: false)
    }
    
    private func createMainButtonsView() {
        mainButtonsView = UIView()
        view.addSubview(mainButtonsView)
        view.bringSubviewToFront(mainButtonsView)
    }
    
    private func createLocalView() {
        localVideo = DSGroupCallLocalVideoView(self)
        localVideo.isHidden = true
        mainButtonsView.addSubview(localVideo)
    }
    
    private func createMainGradients() {
        mainGradient = CAGradientLayer()
        mainGradient.colors = [UIColor(white: 0, alpha: 0.6).cgColor, UIColor.clear.cgColor,UIColor.clear.cgColor,UIColor(white: 0, alpha: 0.6).cgColor]
        mainGradient.locations = [0,0.15,0.80,1]
        mainButtonsView.layer.insertSublayer(mainGradient, above: localVideo.layer)
    }
    
    private func createOngoingCallLabels() {
        titleLabel = UILabel()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textColor = UIColor.white
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2
        mainButtonsView.addSubview(titleLabel)
        
        statusLabel = UILabel()
        statusLabel.textColor = UIColor(white: 1, alpha: 0.8)
        statusLabel.textAlignment = .center
        statusLabel.tag = kViewDontHideTag
        mainButtonsView.addSubview(statusLabel)
    }
    
    private func createOngoingCallControls() {
//        backButton = createButton(type: .plain,image: UIImage(named:"groupcall-close")!, action: #selector(userDidPressBackButton(sender:)))
//        backButton.accessibilityLabel = NSLocalizedString("BACK_BUTTON_LABEL", comment: "Button for going back from call")
//        mainButtonsView.addSubview(backButton)
        
        hangUpButton = createButton(type: .system,image: #imageLiteral(resourceName: "groupcall-hangup"),
                                    action: #selector(userDidPressHangup(sender:)))
        hangUpButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_HANGUP_LABEL",
                                                            comment: "Accessibility label for hang up call")
        let redHangupColor = UIColor(red: 238/255, green: 70/255, blue: 70/255, alpha: 1)
        hangUpButton.backgroundColor = redHangupColor
        hangUpButton.setTitle(NSLocalizedString("END_GROUP_CALL", comment: "Hangup group call button label"), for: .normal)
        hangUpButton.setTitleColor(redHangupColor, for: .normal )
        
        muteSoundButton = createButton(type: .dynamic,image: UIImage(named:"groupcall-mic")!,
                                       action: #selector(userDidPressMute))
        muteSoundButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_MUTE_LABEL", comment: "Accessibility label for muting the microphone")
        muteSoundButton.setTitle(NSLocalizedString("MIC_GROUP_CALL", comment: "Group mic group call button label"), for: .normal)
        
        showVideoButton = createButton(type: .dynamic,image: UIImage(named:"groupcall-video")!,
                                       action: #selector(userDidPressVideo))
        showVideoButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_SWITCH_TO_AUDIO_LABEL", comment: "Accessibility label to switch to audio only")
        showVideoButton.setTitle(NSLocalizedString("VIDEO_GROUP_CALL", comment: "Group video button label in the group call"), for: .normal)
        
        audioSourceButton = DSVideoCallAudioSourceButton()
        audioSourceButton.addTarget(self, action: #selector(userDidPressAudioSource), for: .touchUpInside)
        audioSourceButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_AUDIO_SOURCE_LABEL",
                                                                 comment: "Accessibility label for selection the audio source")
        mainButtonsView.addSubview(audioSourceButton)
        audioSourceButton.switchImage(to: .speaker)
        
        flipCameraButton = createButton(type: .plain,image: UIImage(named:"groupcall-flipcamera")!,
                                        action: #selector(userDidPressFlipCamera))
        
        flipCameraButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_SWITCH_CAMERA_DIRECTION", comment: "Accessibility label to toggle front- vs. rear-facing camera")
        flipCameraButton.isHidden = true
        mainButtonsView.addSubview(flipCameraButton)
        
        ongoingCallControls = UIStackView(arrangedSubviews: [muteSoundButton,hangUpButton,showVideoButton])
        ongoingCallControls.axis = .horizontal
        ongoingCallControls.alignment = .center
        ongoingCallControls.spacing = 25
        
        mainButtonsView.addSubview(ongoingCallControls)
        mainButtonsView.bringSubviewToFront(ongoingCallControls)
        updateAudioSourceButton()
    }
    
    private func createAvatars() {
        mutedAvatarViewStack = UIStackView()
        mutedAvatarViewStack.axis = .vertical
        mutedAvatarViewStack.alignment = .top
        mutedAvatarViewStack.spacing = 5
        mutedAvatarViewStack.tag = kViewDontHideTag
        mainButtonsView.addSubview(mutedAvatarViewStack)
        
        let collectionBackground = UIImageView(image:UIImage(named: "groupcall-background"))
        localAvatarView = DSVideoCallAvatar(number: roomEntity.localNumber,type: .local)
        localAvatarView.isHidden = true
        collectionBackground.addSubview(localAvatarView)
        collectionView.backgroundView = collectionBackground
    }
    
    private func createViewsConstraints() {
        let ongoingMargin = 10
        let localVideoBottomInset: CGFloat = -30
        let localVideoRightInset: CGFloat = 10
        let smallOffset: CGFloat = 5
        let topMargin: CGFloat = 44
        let smallButtonsSize = CGSize(width: 40, height: 40)
        
        let screenBounds = UIScreen.main.bounds
        let localVideoAspect: CGFloat = 1.3333 //TODO: Change later
        let fullLocalVideoWidth = screenBounds.height * localVideoAspect
        
        collectionView.autoPinEdgesToSuperviewEdges()
        
        mainButtonsView.autoPinEdgesToSuperviewEdges()
        
//        backButton.autoPinEdge(toSuperviewEdge: .top, withInset: topMargin)
//        backButton.autoPinEdge(toSuperviewEdge: .leading, withInset: CGFloat(ongoingMargin))
//        backButton.autoSetDimensions(to: smallButtonsSize)
        
        audioSourceButton.autoPinEdge(toSuperviewEdge: .top, withInset: topMargin)
        audioSourceButton.autoPinEdge(toSuperviewEdge: .trailing, withInset: CGFloat(ongoingMargin))
        audioSourceButton.autoSetDimensions(to: smallButtonsSize)
        
        flipCameraButton.autoPinEdge(.trailing, to: .leading, of: audioSourceButton, withOffset: -smallOffset)
        flipCameraButton.autoPinEdge(toSuperviewEdge: .top, withInset: topMargin)
        flipCameraButton.autoSetDimensions(to: smallButtonsSize)
        
        titleLabel.autoPinEdge(toSuperviewEdge: .top, withInset: topMargin)
        titleLabel.autoPinEdge(.leading, to: .trailing, of: self.view, withOffset: smallOffset)
        titleLabel.autoPinEdge(.trailing, to: .leading, of: audioSourceButton, withOffset: -smallOffset)
        titleLabel.setContentHuggingVerticalHigh()
        titleLabel.autoHCenterInSuperview()
        
        statusLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: CGFloat(ongoingMargin))
        statusLabel.autoAlignAxis(.vertical, toSameAxisOf: titleLabel)
        
        ongoingCallControls.autoPinEdge(.bottom, to: .top, of: statusLabel, withOffset: CGFloat(-ongoingMargin * 2))
        ongoingCallControls.autoHCenterInSuperview()
        
        mutedAvatarViewStack.autoPinEdge(toSuperviewEdge: .trailing, withInset: CGFloat(ongoingMargin))
        mutedAvatarViewStack.autoPinEdge(.top, to: .bottom, of: audioSourceButton, withOffset: CGFloat(ongoingMargin))
        
        localAvatarView.autoCenterInSuperview()
        localAvatarView.autoSetDimensions(to: CGSize(width: kGroupCallBackgroudAvatarSize, height: kGroupCallBackgroudAvatarSize))
        
        let topLocal = localVideo.autoPinEdge(.bottom, to: .top, of: ongoingCallControls, withOffset: localVideoBottomInset)
        let rightLocal = localVideo.autoPinEdge(toSuperviewEdge: .trailing, withInset: localVideoRightInset)
        localVideoConstraintsArray.append(contentsOf: [topLocal,rightLocal])
        
        let fullCenter = localVideo.autoCenterInSuperview()
        let fullDimesions = localVideo.autoSetDimensions(to: CGSize(width: fullLocalVideoWidth, height: screenBounds.height))
        let allFullConstraits = fullCenter + fullDimesions
        
        NSLayoutConstraint.deactivate(allFullConstraits)
        fullLocalVideoConstraintsArray.append(contentsOf: allFullConstraits)
    }
    
    private func layoutSublayers() {
        mainGradient.frame = view.bounds
    }
    
    private func updateHideButtonsTimer(buttonsHidden: Bool) {
        invalidateHideButtonsTimerIfNeeded()
        if !buttonsHidden {
            hideMainButtonsViewTimer = Timer(timeInterval: 8, target: self, selector: #selector(hideButtonsTimerDidFinish), userInfo: nil, repeats: false)
            RunLoop.current.add(hideMainButtonsViewTimer!, forMode: .common)
        }
    }
    
    private func createGestureRecognizers() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapMainView))
        mainButtonsView?.addGestureRecognizer(tapGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(userPannedView(gesture:)))
        panGesture.delegate = self
        localVideo.addGestureRecognizer(panGesture)
        localVideo.isUserInteractionEnabled = true
    }
    
    // DSGroupCallLocalVideoView Delegate
    func dimensionChanged(_ capturer: DSGroupCallLocalVideoView, dimensions: CGSize) {
        setLocalViewDimension(dimensions)
        if localVideo.isHidden {
            localVideo.isHidden = false
        }
    }
    
    private func setLocalViewDimension(_ dimension: CGSize) {// TOOD: Switch aspect to constant if all devices can capture in 640x480
        AssertIsOnMainThread()
        
        guard localVideoConstraintsArray.count < 4 else { return }
        let aspect = dimension.height / dimension.width
        let videoHeight: CGFloat = 130
        let resultDimension = CGSize(width: videoHeight * aspect, height: videoHeight)
        
        let dimensionConstraint = localVideo!.autoSetDimensions(to: resultDimension)
        localVideoConstraintsArray.append(contentsOf: dimensionConstraint)
    }
    
    private func updateMutedAvatarViews() {
        AssertIsOnMainThread()
        let arrangedSubviews = mutedAvatarViewStack.arrangedSubviews as! [DSVideoCallAvatar]
        let callMembersSet = Set([String](rendererOfNumber.keys))
        let arrangedNumberSet = arrangedSubviews.map { $0.getNumber() }
        
        let difference = callMembersSet.symmetricDifference(arrangedNumberSet)
        
        difference.forEach { diffNumber in
            if callMembersSet.contains(diffNumber) {
                let avatarView = DSVideoCallAvatar(number: diffNumber,type: .remote)
                avatarView.isHidden = true
                mutedAvatarViewStack.addArrangedSubview(avatarView)
            } else {
                if let avatarViewToDelete = arrangedSubviews.first(where: {$0.getNumber() == diffNumber}) {
                    mutedAvatarViewStack.removeArrangedSubview(avatarViewToDelete)
                    avatarViewToDelete.isHidden = true
                }
            }
        }
    }
    
    private func updateAudioSourceButton() {
        AssertIsOnMainThread()
        // With bluetooth, button does not stay selected. Pressing it pops an actionsheet
        // and the button should immediately "unselect".
        audioSourceButton.isSelected = false
        
        audioSourceButton.switchImage(to: currentOutputDeviceType)
    }
}

// MARK: - DSConferenceServiceDelegate -
extension DSGroupCallViewController: DSGroupCallServiceDelegate {
    public func endCallView() {
        self.dismissImmediately()
    }
    
    public func handleRemoteAudioState(is active: Bool, number: String) {
        roomEntity.saveAudioVideoStates(for: number, isVideo: false, active: active)
        if let visibleCells = collectionView.visibleCells as? [DSGroupCallCollectionCell],
            let cell = visibleCells.first(where: {$0.getNumber() == number }) {
            cell.soundIsActive(active)
        }
        
        let arrangedSubviews = mutedAvatarViewStack.arrangedSubviews as! [DSVideoCallAvatar]
        let avatarToChange = arrangedSubviews.first(where: {$0.getNumber() == number})
        avatarToChange?.mute(is: active)
    }
    
    public func handleRemoteVideoState(is active: Bool, number: String) {
        roomEntity.saveAudioVideoStates(for: number, isVideo: true, active: active)
        checkIfLocalVideoOnFull()
        
        updateActiveRemoteNumbers()
        
        let arrangedSubviews = mutedAvatarViewStack.arrangedSubviews as! [DSVideoCallAvatar]
        let avatarView = arrangedSubviews.first(where: {$0.getNumber() == number})
        avatarView?.isHidden = active
    }
    
    public func didChangeStreamsList(remoteStreams: [String : RTCMediaStream]) {
        self.rendererOfNumber = remoteStreams
        // Send message to CallKit that call is started
        if remoteStreams.count >= 1 && isFirstParticipant {
            if let call = roomEntity.call {
                // Set local video to true here
                // because CallAudioService ensureProperAudioSesion(:) selects proper audio session
                // depending on this variable
                call.hasLocalVideo = true
                call.state = .connected
                startCallDurationTimer()
            }
            callUIAdapter.recipientAcceptedCall(roomEntity)
            isFirstParticipant = false
        }
    }
    
    public func didAddLocalCameraCapturer(cameraVideoCapturer: RTCCameraVideoCapturer) {
        Logger.debug(" Added Local Stream")
        AssertIsOnMainThread()
        
        localVideo.setCameraCapturer(cameraVideoCapturer)
        localVideo.startCapture()
    }
    
    public func didAddLocalFileCapturer(capturer: RTCFileVideoCapturer) {
        let captureController = DSNBMFileCaptureController(capturer: capturer)
        captureController.startCapture()
    }
    
    public func didFailWithError(error: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: error, duration: .middle)
            snackbar.show()
        })
    }
    
    public func resetUIState() {
        updateAudioSourceButton()
        muteSoundButton.isSelected = false
        showVideoButton.isSelected = false
        roomEntity.call.state = .dialing
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.roomEntity.call.state = .connected // Changing state reensures audio session
        }
    }
    
    public func iceStatusChanged(state: RTCIceConnectionState, number: String) {
        //TODO: Delete if not using
    }
    private func startCallDurationTimer() {
        if callDurationTimer == nil {
            let kDurationUpdateFrequencySeconds = 1 / 20.0
            callDurationTimer = WeakTimer.scheduledTimer(timeInterval: TimeInterval(kDurationUpdateFrequencySeconds),
                                                         target: self,
                                                         userInfo: nil,
                                                         repeats: true) {[weak self] _ in
                                                            self?.updateCallDuration()
            }
            
        } else {
            callDurationTimer?.invalidate()
            callDurationTimer = nil
        }
        
    }
    
    private func updateCallDuration() {
        let call = roomEntity.call!
        if call.state == .connected {
            let callDuration = call.connectionDuration()
            let callDurationDate = Date(timeIntervalSinceReferenceDate: callDuration)
            if dateFormatter == nil {
                dateFormatter = DateFormatter()
                dateFormatter!.dateFormat = "HH:mm:ss"
                dateFormatter!.timeZone = TimeZone(identifier: "UTC")!
            }
            var formattedDate = dateFormatter!.string(from: callDurationDate)
            if formattedDate.hasPrefix("00:") {
                // Don't show the "hours" portion of the date format unless the
                // call duration is at least 1 hour.
                formattedDate = String(formattedDate[formattedDate.index(formattedDate.startIndex, offsetBy: 3)...])
            } else {
                // If showing the "hours" portion of the date format, strip any leading
                // zeroes.
                if formattedDate.hasPrefix("0") {
                    formattedDate = String(formattedDate[formattedDate.index(formattedDate.startIndex, offsetBy: 1)...])
                }
            }
            statusLabel.text = formattedDate
        }
    }
    
    private func checkIfLocalVideoOnFull() {
        guard rendererOfNumber.count != 0 && isVideoOn else {
            isLocalVideoOnFull = false
            return
        }
        isLocalVideoOnFull = roomEntity.noVideoList.count == rendererOfNumber.count ? true : false
        Logger.debug("Novideo count \(roomEntity.noVideoList.count)")
        Logger.debug("Members count \(rendererOfNumber.count)")
        Logger.debug("Full screen is \(isLocalVideoOnFull)")
    }
    
    private func switchBetweenLocalVideos() {
        AssertIsOnMainThread()
        
        if isLocalVideoOnFull  {
            flipCameraButton.isHidden = false
            NSLayoutConstraint.deactivate(localVideoConstraintsArray)
            NSLayoutConstraint.activate(fullLocalVideoConstraintsArray)
        } else {
            flipCameraButton.isHidden = true
            NSLayoutConstraint.deactivate(fullLocalVideoConstraintsArray)
            NSLayoutConstraint.activate(localVideoConstraintsArray)
        }
    }
    
    private func updateActiveRemoteNumbers() {
        var activeNumbers = [String](self.rendererOfNumber.keys)
        activeNumbers = activeNumbers.filter({ (number) -> Bool in
            if roomEntity.noVideoList.contains(number) {
                return false
            }
            return true
        })
        
        self.activeRemoteNumbers = activeNumbers
    }
}

//MARK: - Collection View Methods -
extension DSGroupCallViewController {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activeRemoteNumbers.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DSGroupCallCollectionCell
        let phoneNumber = activeRemoteNumbers[indexPath.row]
        let isSoundOn = !roomEntity.noAudioList.contains(phoneNumber)
        let cellCenter = collectionView.layoutAttributesForItem(at: indexPath)?.center
        
        if let mediaStream = rendererOfNumber[phoneNumber],let videoTrack = mediaStream.videoTracks.first {
            let remoteVideo = DSRemoteVideoView()
            videoTrack.add(remoteVideo)
            
            cell.setVideoView(to: remoteVideo,number: phoneNumber,center: cellCenter)
            cell.soundIsActive(isSoundOn)
        }
        return cell
    }
}

// MARK: - Call Control Methods -
extension DSGroupCallViewController {
//    @objc func userDidPressBackButton(sender: UIButton) {
////        OWSWindowManager.shared.leaveCallView()
//    }
    
    @objc func userDidPressHangup(sender: UIButton) {
        callUIAdapter.stopOutgoingCall(roomEntity)
        dismissImmediately()
    }
    
    @objc func userDidPressVideo(sender: UIButton) {
        AssertIsOnMainThread()
        updateHideButtonsTimer(buttonsHidden: false)
        let isVideo = !sender.isSelected
        sender.isSelected = isVideo
        
        roomEntity.saveAudioVideoStates(for: roomEntity.localNumber, isVideo: true, active: !isVideo)
        
        isVideoOn = !isVideo
        
        callUIAdapter.showVideo(roomEntity,isVideo: isVideoOn)
        Logger.info(" Video is \(isVideoOn)")
    }
    
    @objc func userDidPressMute(sender muteButton: UIButton) {
        AssertIsOnMainThread()
        updateHideButtonsTimer(buttonsHidden: false)
        
        muteButton.isSelected = !muteButton.isSelected
        let muted = muteButton.isSelected
        
        roomEntity.saveAudioVideoStates(for: roomEntity.localNumber, isVideo: false, active: !muted)
        
        self.callUIAdapter.setIsMuted(self.roomEntity,isMuted: muted)

        Logger.info(" Muted is \(muted)")
    }
    
    @objc func userDidPressAudioSource(sender audioSource: UIButton) {
        AssertIsOnMainThread()
        updateHideButtonsTimer(buttonsHidden: false)
        
        if self.hasAlternateAudioSources {
            presentAudioSourcePicker()
            return
        }
        isSpeakerPhoneEnabled = !isSpeakerPhoneEnabled
        callUIAdapter.audioService.requestSpeakerphone(isEnabled: isSpeakerPhoneEnabled)
    }
    
    @objc func userDidPressFlipCamera(_ sender: UIButton) {
        updateHideButtonsTimer(buttonsHidden: false)
        localVideo.userDidPressFlipCamera(sender: sender)
    }
    
    @objc func hideButtonsTimerDidFinish() {
        Logger.debug("Hide buttons timer did finish")
        invalidateHideButtonsTimerIfNeeded()
        userDidTapMainView()
    }
    private func invalidateHideButtonsTimerIfNeeded() {
        guard hideMainButtonsViewTimer != nil else { return }
        hideMainButtonsViewTimer?.invalidate()
        hideMainButtonsViewTimer = nil
    }
    
    func presentAudioSourcePicker() {
        AssertIsOnMainThread()
        
        updateAvailableInputs()
        guard let call = roomEntity.call else { return }
        
        let currentAudioSource = callUIAdapter.audioService.currentAudioSource(call: call)
//        let popup = DSCallPopupView(allSources: self.appropriateAudioSources, current: currentAudioSource, shouldBeDraggable: true, contentViewHeight: nil)
//        popup.modalPresentationStyle = .custom
//        popup.delegate = self
//        present(popup, animated: false, completion: nil)
    }
}

// MARK: - Animations -
extension DSGroupCallViewController: UIGestureRecognizerDelegate {
    @objc
    func userPannedView(gesture: UIPanGestureRecognizer) {
        AssertIsOnMainThread()
        
        guard isMainButtonViewHidden,
            gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed,
            let dragView = gesture.view else { return }
        
        let pan = gesture.translation(in: self.mainButtonsView)
        var dragViewTransformOrigin = dragView.frame
        dragViewTransformOrigin.origin = dragViewTransformOrigin.origin.plus(pan)
        
        if view.bounds.contains(dragViewTransformOrigin) {
            UIView.animate(withDuration: 0.3, delay: 0,
                           options: [.allowUserInteraction], animations: {
                            gesture.view!.center = CGPoint(x:dragView.center.x + pan.x, y:dragView.center.y + pan.y)
                            self.localVideoConstraintsArray[0].constant += pan.y
                            self.localVideoConstraintsArray[1].constant += pan.x
            }, completion: nil)
        }
        gesture.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @objc func userDidTapMainView() {
        AssertIsOnMainThread()
        
        let intVisible: CGFloat = isMainButtonViewHidden ? 1 : 0
        UIView.animate(withDuration: 0.3, delay: 0,
                       options: [.curveEaseInOut,.allowUserInteraction],
                       animations: { [weak self] in
                        self?.mainButtonsView.subviews.forEach { if ($0.tag != kViewDontHideTag)  {$0.alpha = intVisible} }
                        self?.mainGradient.opacity = Float(intVisible)
                        
            }, completion: { _ in
                self.isMainButtonViewHidden = !self.isMainButtonViewHidden
        })
    }
}

//MARK: - Helpers methods -
extension DSGroupCallViewController {
    private func createButton(type: DSVideoCallButtonType,image: UIImage?, action: Selector) -> DSVideoCallButton {
        let button = DSVideoCallButton(type)
        button.addTarget(self, action: action, for: .touchUpInside)
        
        if image != nil {
            button.setCustomBackgroundImage(image: image!)
        }
        return button
    }
    
    public func dismissImmediately() {
        self.dismiss(animated: true, completion: nil)
//        if CallViewController.kShowCallViewOnSeparateWindow {
//            OWSWindowManager.shared.endCall(self)
//        } else {
//        }
        Logger.info("Close call view")
    }
}

// MARK: - AudioSource methods -
extension DSGroupCallViewController: CallAudioServiceDelegate
   // ,DSCallPopupDelegate
{
    
    func callAudioService(_ callAudioService: CallAudioService, didUpdateIsSpeakerphoneEnabled isEnabled: Bool) {
        //update audiosource
        self.isSpeakerPhoneEnabled = isEnabled
        Logger.debug("Speakerphone is \(isEnabled)")
    }
    
    func callAudioServiceDidChangeAudioSession(_ callAudioService: CallAudioService) {
        updateAvailableInputs()
    }
    
    @objc func audioRouteChangeListener(notification: Notification) {
        updateCurrentOutputDevice()
        updateAvailableInputs()
        Logger.debug("BT Device updated")
    }
    
    var hasAlternateAudioSources: Bool {
        Logger.info("available audio sources: \(allAudioSources)")
        // internal mic and speakerphone will be the first two, any more than one indicates e.g. an attached bluetooth device.
        
        // TODO is this sufficient? Are their devices w/ bluetooth but no external speaker? e.g. ipod?
        return allAudioSources.count > 2
    }
    
    var appropriateAudioSources: Set<AudioSource> {
        if isVideoOn {
            let appropriateForVideo = allAudioSources.filter { audioSource in
                if audioSource.isBuiltInSpeaker {
                    return true
                } else {
                    guard let portDescription = audioSource.portDescription else {
                        owsFailDebug("Only built in speaker should be lacking a port description.")
                        return false
                    }
                    
                    // Don't use receiver when video is enabled. Only bluetooth or speaker
                    return portDescription.portType != AVAudioSession.Port.builtInMic
                }
            }
            return Set(appropriateForVideo)
        } else {
            return allAudioSources
        }
    }
    
    private func updateCurrentOutputDevice() {
        let inputs = avAudioSession.currentRoute.inputs
        let outputs = avAudioSession.currentRoute.outputs
        let inOutRoutes = inputs + outputs
        self.currentOutputDeviceType = inOutRoutes.findCallAudioSourceType()
    }
    
    private func updateAvailableInputs() {
        self.allAudioSources = allAudioSources.filter {
            if $0.isBuiltInEarPiece == true || $0.isBuiltInSpeaker == true {
                return true
            }
            return false
        }
        let available = callUIAdapter.audioService.availableInputs
        allAudioSources.formUnion(available)
        
        DispatchQueue.main.async {
            self.updateAudioSourceButton()
        }
    }
    
    internal func pickerSelectedAudioSource(audioSource: AudioSource) {
        Logger.debug("Selected audiosource via popup \(audioSource.localizedName)")
        self.callUIAdapter.setAudioSource(call: roomEntity.call!, audioSource: audioSource)
    }
}

extension Array where Element: AVAudioSessionPortDescription {
    func findCallAudioSourceType() -> DSVideoCallAudioSourceType {
        let isBT = self.first(where: {
            $0.portType == .bluetoothA2DP || $0.portType == .bluetoothHFP ||
                $0.portType == .airPlay || $0.portType == .carAudio})
        if  isBT != nil {
            return .bluetooth
        }
        
        let isHeadphonesInput = self.first(where: {$0.portType == .headsetMic || $0.portType == .lineIn})
        let isHeadphonesOutput = self.first(where: {$0.portType == .headphones || $0.portType == .lineOut})
        if isHeadphonesInput != nil || isHeadphonesOutput != nil {
            return .headset
        }
        
        let isEarpiece = self.first(where: { $0.portType == .builtInReceiver })
        if isEarpiece != nil {
            return .earpiece
        }
        return .speaker
    }
}
