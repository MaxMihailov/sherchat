//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation
import UIKit
import CallKit
import AVFoundation


class DSCallKitCallUIAdaptee:NSObject, GroupCallUIAdaptee, CXProviderDelegate {
    
    private let callManager: DSCallKitCallManager
    internal let callService: DSGroupCallService
    
    private let showNamesOnCallScreen: Bool
    private let provider: CXProvider
//    private let audioActivity: AudioActivity
    
    let avAudioSession = AVAudioSession.sharedInstance()
    
    // CallKit handles incoming ringer stop/start for us. Yay!
    let hasManualRinger = false
    
    // Instantiating more than one CXProvider can cause us to miss call transactions, so
    // we maintain the provider across Adaptees using a singleton pattern
    private static var _sharedGroupProvider: CXProvider?
    class func sharedGroupProvider(useSystemCallLog: Bool) -> CXProvider {
        let configuration = buildProviderConfiguration(useSystemCallLog: useSystemCallLog)
        
        if let sharedProvider = self._sharedGroupProvider {
            sharedProvider.configuration = configuration
            return sharedProvider
        } else {
            SwiftSingletons.register(self)
            let provider = CXProvider(configuration: configuration)
            _sharedGroupProvider = provider
            return provider
        }
    }
    
    // The app's provider configuration, representing its CallKit capabilities
    class func buildProviderConfiguration(useSystemCallLog: Bool) -> CXProviderConfiguration {
        let localizedName = NSLocalizedString("APPLICATION_NAME", comment: "Name of application")
        let providerConfiguration = CXProviderConfiguration(localizedName: localizedName)
        
        providerConfiguration.supportsVideo = true
        
        providerConfiguration.maximumCallGroups = 2
        
        providerConfiguration.maximumCallsPerCallGroup = 2
        
        providerConfiguration.supportedHandleTypes = [.phoneNumber, .generic]
//
        let iconMaskImage = UIImage(named: "AppIcon")!
        providerConfiguration.iconTemplateImageData = iconMaskImage.pngData()
        
        // We don't set the ringtoneSound property, so that we use either the
        // default iOS ringtone OR the custom ringtone associated with this user's
        // system contact, if possible (iOS 11 or later).
        
        if #available(iOS 11.0, *) {
            providerConfiguration.includesCallsInRecents = useSystemCallLog
        } else {
            // not configurable for iOS10+
            assert(useSystemCallLog)
        }
        
        return providerConfiguration
    }

    
    init(callService: DSGroupCallService, showNamesOnCallScreen: Bool, useSystemCallLog: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("")
        
        self.callManager = DSCallKitCallManager(showNamesOnCallScreen: showNamesOnCallScreen)
        self.callService = callService
        
        self.provider = type(of: self).sharedGroupProvider(useSystemCallLog: useSystemCallLog)
//
//        self.audioActivity = AudioActivity(audioDescription: "[DSCallKitCallUIAdaptee]", behavior: .call)
        self.showNamesOnCallScreen = showNamesOnCallScreen
        
        super.init()
        provider.setDelegate(self, queue: nil)
        // We cannot assert singleton here, because this class gets rebuilt when the user changes relevant call settings
    }
    
    // MARK: Dependencies
    
//    var audioSession: OWSAudioSession {
//        return Environment.shared.audioSession
//    }
    
    func reportIncomingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        let displayName = roomEntity.displayNameForGroupCall
        // Construct a CXCallUpdate describing the incoming call, including the caller.
        let update = CXCallUpdate()
        
        if showNamesOnCallScreen {
            update.localizedCallerName = displayName
            update.remoteHandle = CXHandle(type: .generic, value: displayName)
        } else {
            let callKitId = DSCallKitCallManager.kAnonymousCallHandlePrefix + roomEntity.call.localId.uuidString
            update.remoteHandle = CXHandle(type: .generic, value: roomEntity.call.localId.uuidString)
//            CallKitIdStore.setAddress(roomEntity.call.remoteAddress, forCallKitId: callKitId)
            update.localizedCallerName = NSLocalizedString("CALLKIT_ANONYMOUS_CONTACT_NAME", comment: "The generic name used for calls if CallKit privacy is enabled")
        }
        
        update.hasVideo = true
        
        disableUnsupportedFeatures(callUpdate: update)
        
        // Report the incoming call to the system
        provider.reportNewIncomingCall(with: roomEntity.call.localId, update: update) { error in
            /*
             Only add incoming call to the app's list of calls if the call was allowed (i.e. there was no error)
             since calls may be "denied" for various legitimate reasons. See CXErrorCodeIncomingCallError.
             */
            guard error == nil else {
                Logger.error("failed to report new incoming call, error: \(error!)")
                return
            }
            
            self.callManager.addRoom(roomEntity)
        }
        Logger.debug("\(roomEntity.call.localId) dstar callkit local id")
    }
    
    func startOutgoingCall(_ roomEntity: DSRoomEntity){
        AssertIsOnMainThread()
        
        
        // make sure we don't terminate audio session during call
//        _ = self.audioSession.startAudioActivity(roomEntity.call.audioActivity)
        
        // Add the new outgoing call to the app's list of calls.
        // So we can find it in the provider delegate callbacks.
        callManager.addRoom(roomEntity)
        callManager.startCall(roomEntity)
    }
    
    func stopOutgoingCall(_ roomEntity: DSRoomEntity) {
        callManager.localHangup(call: roomEntity.call)
    }
    
    func recipientAcceptedCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        Logger.info("")
        
        self.provider.reportOutgoingCall(with: roomEntity.call.localId, connectedAt: nil)
        
        let update = CXCallUpdate()
        disableUnsupportedFeatures(callUpdate: update)
        
        provider.reportCall(with: roomEntity.call.localId, updated: update)
    }
    
    
    func setIsMuted(_ roomEntity: DSRoomEntity,isMuted: Bool) {
        AssertIsOnMainThread()
        Logger.info("")
        
        callManager.setIsMuted(call: roomEntity.call, isMuted: isMuted)
    }
    
    func showVideo(_ roomEntity: DSRoomEntity,isVideo: Bool) {
        AssertIsOnMainThread()
        
        let update = CXCallUpdate()
        update.hasVideo = isVideo
        //Update the CallKit UI
        provider.reportCall(with: roomEntity.call.localId, updated: update)
    }
    
    func flipCamera(isFront: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        AssertIsOnMainThread()
        Logger.info("CXStartCallAction")
        
        guard let roomEntity = callManager.roomWithLocalId(action.callUUID) else {
            Logger.error("unable to find call")
            return
        }
        let call = roomEntity.call!

        //  Start call in call service
        self.callService.initializeManager(with: roomEntity)
        
        action.fulfill()
        self.provider.reportOutgoingCall(with: call.localId, startedConnectingAt: nil)
        
        // Update the name used in the CallKit UI for outgoing calls when the user prefers not to show names
        // in ther notifications
        if !showNamesOnCallScreen {
            let update = CXCallUpdate()
            update.localizedCallerName = NSLocalizedString("CALLKIT_ANONYMOUS_CONTACT_NAME",
                                                           comment: "The generic name used for calls if CallKit privacy is enabled")
            provider.reportCall(with: call.localId, updated: update)
        }
    }
    
    //MARK: - CXProvider methods -
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        AssertIsOnMainThread()
        
        Logger.info("Received CXAnswerCallAction")
        guard let roomEntity = callManager.roomWithLocalId(action.callUUID) else {
            Logger.error("trying to answer unknown call with localId (or roomEntity missing): \(action.callUUID)")
            action.fail()
            return
        }

        self.callService.initializeManager(with: roomEntity)
        self.showCall(roomEntity)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: { // fix no sound on incoming call IOS12
            action.fulfill()
        })
    }
    
    
    public func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        AssertIsOnMainThread()
        
        Logger.info("Received CXEndCallAction")
        guard let roomEntity = callManager.roomWithLocalId(action.callUUID) else {
            Logger.error("trying to end unknown call with localId: \(action.callUUID)")
            action.fail()
            return
        }
        
        self.callService.deinitializeRoomManager()
        // Signal to the system that the action has been successfully performed.
        action.fulfill()
        
        // Remove the ended call from the app's list of calls.
        self.callManager.removeRoom(roomEntity)
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        AssertIsOnMainThread()
        
        Logger.debug("Activated audio session callkit")
//        _ = self.audioSession.startAudioActivity(self.audioActivity)
//        self.audioSession.isRTCAudioEnabled = true
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        AssertIsOnMainThread()
        
        Logger.debug("Deactivad audio session callkit")
//        self.audioSession.isRTCAudioEnabled = false
//        self.audioSession.endAudioActivity(self.audioActivity)
    }
    
    func providerDidReset(_ provider: CXProvider) {
        AssertIsOnMainThread()
        Logger.info("")
        
        // End any ongoing calls if the provider resets, and remove them from the app's list of calls,
        // since they are no longer valid.
        self.callService.deinitializeRoomManager()
        
        // Remove all calls from the app's list of calls.
        callManager.removeAllRooms()
    }
    
    public func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        AssertIsOnMainThread()
        
        Logger.warn("unimplemented \(#function) for CXSetGroupCallAction")
    }
    
    public func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        AssertIsOnMainThread()
        Logger.info("Received \(#function) CXSetMutedCallAction")
        
        guard let roomEntity = callManager.roomWithLocalId(action.callUUID) else {
            Logger.error("trying to set muted unknown call with localId: \(action.callUUID)")
            action.fail()
            return
        }
        
//        self.callService.enableAudio(enable: !action.isMuted)
        action.fail()
    }
    
    public func provider(_ provider: CXProvider, perform action: CXSetGroupCallAction) {
        AssertIsOnMainThread()
        
        Logger.warn("unimplemented \(#function) for CXSetGroupCallAction")
    }
    
    public func provider(_ provider: CXProvider, perform action: CXPlayDTMFCallAction) {
        AssertIsOnMainThread()
        
        Logger.warn("unimplemented \(#function) for CXPlayDTMFCallAction")
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        AssertIsOnMainThread()
        
        owsFailDebug("Timed out while performing \(action)")
        
        // React to the action timeout if necessary, such as showing an error UI.
    }
    func remoteDidHangupCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        Logger.info("")
        
        provider.reportCall(with: roomEntity.call.localId, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
    }
    
    // Called from CallService after call has ended to clean up any remaining CallKit call state.
    func failCall(_ roomEntity: DSRoomEntity,error: CallError) {
        AssertIsOnMainThread()
        Logger.info("")
        
        let call = roomEntity.call!
        switch error {
        case .timeout(description: _):
            provider.reportCall(with: call.localId, endedAt: Date(), reason: CXCallEndedReason.unanswered)
        default:
            provider.reportCall(with: call.localId, endedAt: Date(), reason: CXCallEndedReason.remoteEnded)
        }
        
        self.callManager.removeRoom(roomEntity)
    }
    
    private func disableUnsupportedFeatures(callUpdate: CXCallUpdate) {
        // Call Holding is failing to restart audio when "swapping" calls on the CallKit screen
        // until user returns to in-app call screen.
        callUpdate.supportsHolding = false
        
        // Not yet supported
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        
        // Is there any reason to support this?
        callUpdate.supportsDTMF = false
    }
}
