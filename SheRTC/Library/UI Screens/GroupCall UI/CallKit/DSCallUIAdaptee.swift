//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation

protocol GroupCallUIAdaptee {
//    var notificationPresenter: NotificationPresenter { get }
    var callService: DSGroupCallService { get }
    var hasManualRinger: Bool { get }

    func startAndShowOutgoingCall(_ roomEntity: DSRoomEntity)
    func startOutgoingCall(_ roomEntity: DSRoomEntity)
    func reportIncomingCall(_ roomEntity: DSRoomEntity)
    func stopOutgoingCall(_ roomEntity: DSRoomEntity)
    func remoteDidHangupCall(_ roomEntity: DSRoomEntity)
    func recipientAcceptedCall(_ roomEntity: DSRoomEntity)
    
    func showVideo(_ roomEntity: DSRoomEntity,isVideo: Bool)
    func setIsMuted(_ roomEntity: DSRoomEntity,isMuted: Bool)
    func flipCamera(isFront: Bool)
    
    func reportMissedCall(_ call: CallModel, callerName: String)
    func failCall(_ roomEntity: DSRoomEntity,error: CallError)
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension GroupCallUIAdaptee {
    internal func showCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()

        let callViewController = DSGroupCallViewController(room: roomEntity)
        callViewController.modalPresentationStyle = .fullScreen
        guard let vc = UIApplication.topViewController() else {return}
        
        vc.present(callViewController, animated: true, completion: nil)
    }
    internal func reportMissedCall(_ call: CallModel, callerName: String) {
        AssertIsOnMainThread()

//        notificationPresenter.presentMissedCall(call, callerName: callerName)
    }

    internal func startAndShowOutgoingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()

        self.startOutgoingCall(roomEntity)
        self.showCall(roomEntity)
    }

}

public class DSCallUIAdapter: NSObject {
    let callService: DSGroupCallService
    
    lazy var adaptee: GroupCallUIAdaptee = {
         if Platform.isSimulator {
            Logger.info("choosing non-callkit adaptee.")
            return DSNonCallKitCallUIAdaptee(callService: self.callService)
         } else if false {
            Logger.info("choosing non-callkit adaptee due to locale.")
            return DSNonCallKitCallUIAdaptee(callService: self.callService)
         } else if #available(iOS 11, *) {
            Logger.info("choosing callkit adaptee for iOS11+")
            let showNames = true
            let useSystemCallLog = true

            return DSCallKitCallUIAdaptee(callService: self.callService, showNamesOnCallScreen: showNames, useSystemCallLog: useSystemCallLog)
         } else {
            Logger.info("choosing non-callkit adaptee")
            return DSNonCallKitCallUIAdaptee(callService: self.callService)
        }
    }()

    lazy var audioService: CallAudioService = {
        return CallAudioService(handleRinging: adaptee.hasManualRinger)
    }()
    
    // MARK: Dependencies
    
//    var audioSession: OWSAudioSession {
//        return Environment.shared.audioSession
//    }
    
    public required init(callService: DSGroupCallService) {
        AssertIsOnMainThread()
    
        self.callService = callService
        super.init()
        
        //We cannot assert singleton here, because this class gets rebuilt when the user changes relevant call settings
//        AppReadiness.runNowOrWhenAppDidBecomeReady {
            callService.addObserverAndSyncState(observer: self)
//        }/
    }
    
    @objc public func startAndShowOutgoingCall(roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        adaptee.startAndShowOutgoingCall(roomEntity)
    }
    
    @objc public func reportIncomingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        guard let call = roomEntity.call else {
            owsFailDebug("call")
            return
        }
//         //make sure we don't terminate audio session during call
//        _ = audioSession.startAudioActivity(call.audioActivity)
//
        adaptee.reportIncomingCall(roomEntity)
    }
    
    @objc public func stopOutgoingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()

        adaptee.stopOutgoingCall(roomEntity)
        
        didTerminateCall(roomEntity.call)
    }
    
    @objc public func remoteDidHangupCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        adaptee.remoteDidHangupCall(roomEntity)
    }
    
    @objc public func recipientAcceptedCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        guard let call = roomEntity.call else { return }
        call.state = .connected
        
        adaptee.recipientAcceptedCall(roomEntity)
    }
    
    internal func setIsMuted(_ roomEntity: DSRoomEntity,isMuted: Bool) {
        AssertIsOnMainThread()
        
        self.callService.enableAudio(enable: !isMuted)
        
        adaptee.setIsMuted(roomEntity,isMuted: isMuted)
    }

    internal func reportMissedCall(_ call: CallModel, callerName: String) {
        AssertIsOnMainThread()
        
        adaptee.reportMissedCall(call, callerName: callerName)
    }
    internal func showVideo(_ roomEntity: DSRoomEntity,isVideo: Bool) {
        AssertIsOnMainThread()
        
        self.callService.enableVideo(enable: isVideo)
        adaptee.showVideo(roomEntity ,isVideo: isVideo)
    }
    internal func flipCamera(isFront: Bool) {
        AssertIsOnMainThread()
        
        adaptee.flipCamera(isFront: isFront)
    }
    
    internal func setAudioSource(call: CallModel, audioSource: AudioSource?) {
        AssertIsOnMainThread()
        
        // AudioSource is not handled by CallKit (e.g. there is no CXAction), so we handle it w/o going through the
        // adaptee, relying on the AudioService CallObserver to put the system in a state consistent with the call's
        // assigned property.
        call.audioSource = audioSource
    }
    
    internal func failCall(_ roomEntity: DSRoomEntity,error: CallError) {
        AssertIsOnMainThread()
        
        adaptee.failCall(roomEntity,error: error)
    }
    internal func didTerminateCall(_ call: CallModel?) {
        AssertIsOnMainThread()
        
//        if let call = call {
//            self.audioSession.endAudioActivity(call.audioActivity)
//        }
    }
}

extension DSCallUIAdapter: CallServiceObserver {
    func didUpdateCall(call: CallModel?) {
        AssertIsOnMainThread()
        
        call?.addObserverAndSyncState(observer: audioService)
    }
    
    func didUpdateVideoTracks(call: CallModel?, localCaptureSession: AVCaptureSession?, remoteVideoTrack: RTCVideoTrack?) {
        AssertIsOnMainThread()
        
        audioService.didUpdateVideoTracks(call: call)
    }
}
