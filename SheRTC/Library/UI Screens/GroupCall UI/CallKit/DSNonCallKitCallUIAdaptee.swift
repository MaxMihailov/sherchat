//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation

class DSNonCallKitCallUIAdaptee:NSObject, GroupCallUIAdaptee {
    
    let callService: DSGroupCallService
    
    let avAudioSession = AVAudioSession.sharedInstance()
//    private let audioActivity: AudioActivity
    
    init(callService: DSGroupCallService) {
        self.callService = callService
        super.init()
    }
    
    var hasManualRinger = false
    
//    var audioSession: OWSAudioSession {
//        return Environment.shared.audioSession
//    }
//
    func startOutgoingCall(_ roomEntity: DSRoomEntity){
//        let success = self.audioSession.startAudioActivity(roomEntity.call!.audioActivity)
//        assert(success)
        
//        self.audioSession.isRTCAudioEnabled = true
        self.callService.initializeManager(with: roomEntity)
    }
    
    func reportIncomingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        self.showIncomingCall(roomEntity)
    }
    
    func showIncomingCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        let incomingCallController = DSIncomingCallViewController(room: roomEntity)
        incomingCallController.modalTransitionStyle = .flipHorizontal
//        guard let presentingViewController = UIApplication.shared.frontmostViewController else {
//            owsFailDebug("Front view controller is nil")
//            return
//        }
//        if let presentedViewController = presentingViewController.presentedViewController {
//            presentedViewController.dismiss(animated: false) {
//                presentingViewController.present(incomingCallController, animated: true)
//            }
//        } else {
//            presentingViewController.present(incomingCallController, animated: true)
//        }
    }
    
    func recipientAcceptedCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    func setIsMuted(_ roomEntity: DSRoomEntity,isMuted: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    func showVideo(_ roomEntity: DSRoomEntity, isVideo: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    func reportMissedCall(_ call: CallModel, callerName: String) {
        AssertIsOnMainThread()
        
//        notificationPresenter.presentMissedCall(call, callerName: callerName)
    }
    
    func stopOutgoingCall(_ roomEntity: DSRoomEntity) {
        self.callService.deinitializeRoomManager()
    }
    
    func remoteDidHangupCall(_ roomEntity: DSRoomEntity) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
        
    }
    
    
    func selectAudioSource(isLoud: Bool) {
        DispatchQueue.global().async {
            do {
                try self.avAudioSession.overrideOutputAudioPort( isLoud ? .speaker : .none )
            } catch {
                owsFailDebug("failed to set \(#function) = \(isLoud) with error: \(error)")
            }
        }
    }
    
    internal func flipCamera(isFront: Bool) {
    }
    
    internal func failCall(_ roomEntity: DSRoomEntity,error: CallError) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
}
