//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import UIKit

enum MosaicSegmentStyle {
    case full
    case fiftyFifty
}

class DSConferenceViewLayout: UICollectionViewLayout {
    var contentBounds = CGRect.zero
    var cachedAttributes = [UICollectionViewLayoutAttributes]()
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else {
            return
        }
        cachedAttributes.removeAll()
        contentBounds = CGRect(origin: .zero, size: collectionView.bounds.size)
        
        let count = collectionView.numberOfItems(inSection: 0)
        
        var currentIndex = 0
        var segment: MosaicSegmentStyle = .full
        var lastFrame: CGRect = .zero
        
        let cvWidth = collectionView.frame.size.width
        let cvHeight = collectionView.frame.size.height
        
        while currentIndex < count {
            var heightDivider: CGFloat = 3
            
            switch count {
            case 1:
                heightDivider = 1
                segment = .full
            case 2:
                heightDivider = 2
                segment = .full
            case 3:
                heightDivider = 2
                switch segment {
                case .full:
                    segment = .fiftyFifty
                case .fiftyFifty:
                    segment = .full
                }
            case 4:
                heightDivider = 2
                segment = .fiftyFifty
            case 5:
                switch segment {
                case .full:
                    segment = .fiftyFifty
                case .fiftyFifty:
                    segment = .full
                }
            case 6:
                segment = .fiftyFifty
            default:
                segment = .full
            }
            
            let fullFrame = CGRect(x: 0, y: lastFrame.maxY, width: cvWidth, height: cvHeight / heightDivider)
            var segmentRects = [CGRect]()
            switch segment {
            case .full:
                segmentRects = [fullFrame]
            case .fiftyFifty:
                let horizontalParts = fullFrame.dividedIntegral(fraction: 0.5000, from: .minXEdge)
                segmentRects = [horizontalParts.first, horizontalParts.second]
            }
            
            for rect in segmentRects {
                let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: currentIndex, section: 0))
                attributes.frame = rect
                
                cachedAttributes.append(attributes)
                contentBounds = contentBounds.union(lastFrame)
                
                currentIndex += 1
                lastFrame = rect
            }
        }
    }
    override var collectionViewContentSize: CGSize {
        return contentBounds.size
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let collectionView = collectionView else {
            return false
        }
        return !newBounds.size.equalTo(collectionView.bounds.size)
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cachedAttributes[indexPath.item]
    }
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributesArray = [UICollectionViewLayoutAttributes]()
        guard let lastIndex = cachedAttributes.indices.last, let firstMatchIndex = binSearch(rect,start: 0,end: lastIndex) else { return attributesArray}
        
        for attributes in cachedAttributes[..<firstMatchIndex].reversed() {
            guard attributes.frame.maxY >= rect.minY else {
                break
            }
            attributesArray.append(attributes)
        }
        
        for attributes in cachedAttributes[firstMatchIndex...] {
            guard attributes.frame.minY <= rect.maxY else {
                break
            }
            attributesArray.append(attributes)
        }
        
        return attributesArray
    }
    
    func binSearch(_ rect: CGRect, start: Int, end: Int) -> Int? {
        if end < start { return nil }
        
        let mid = (start + end) / 2
        let attr = cachedAttributes[mid]
        
        if attr.frame.intersects(rect) {
            return mid
        } else {
            if attr.frame.maxY < rect.minY {
                return binSearch(rect, start: (mid + 1), end: end)
            } else {
                return binSearch(rect, start: start, end: (mid - 1))
            }
        }
    }
}
extension CGRect {
    func dividedIntegral(fraction: CGFloat, from fromEdge: CGRectEdge) -> (first: CGRect, second: CGRect) {
        let dimension: CGFloat
        
        switch fromEdge {
        case .minXEdge, .maxXEdge:
            dimension = self.size.width
        case .minYEdge, .maxYEdge:
            dimension = self.size.height
        }
        
        let distance = (dimension * fraction).rounded(.up)
        var slices = self.divided(atDistance: distance, from: fromEdge)
        return (first: slices.slice, second: slices.remainder)
    }
}
