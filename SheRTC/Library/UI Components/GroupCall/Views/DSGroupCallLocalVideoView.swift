//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation

protocol DSGroupCallLocalVideoViewDelegate: class {
    func dimensionChanged(_ capturer:DSGroupCallLocalVideoView, dimensions: CGSize)
}

class DSGroupCallLocalVideoView: UIView {
    weak var delegate: DSGroupCallLocalVideoViewDelegate?
    
    private let previewView = RTCCameraPreviewView()
    private var flipCameraButton: DSVideoCallButton!
    private var captureController: DSNBMCaptureController? = nil
    
    private var localVideoGradient: CAGradientLayer!
    private let gradientView: UIView = UIView()
    
    let kCornerRadius: CGFloat = 10
    
    init(_ delegate: DSGroupCallLocalVideoViewDelegate?) {
        super.init(frame: CGRect.zero)
        self.delegate = delegate
        self.backgroundColor = .darkGray
        
        layoutIfNeeded()
        layer.cornerRadius = kCornerRadius
        layer.masksToBounds = true
        createViews()
        createViewsConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setCameraCapturer(_ capturer: RTCCameraVideoCapturer) {
        previewView.captureSession = capturer.captureSession
        captureController = DSNBMCaptureController(capturer: capturer,usingFrontCamera: true)
    }
    
    public func startCapture() {
        captureController?.startCapture { [weak self] size in
            guard let self = self,let captureSize = size else { return }
            DispatchQueue.main.async {
                self.delegate?.dimensionChanged(self,dimensions: captureSize)
                self.layoutGradient()
            }
        }
    }
    
    public func stopCapture() {
        captureController?.stopCapture()
    }
    
    public func changeCameraPosition(_ isFront: Bool) {
        guard captureController?.isCaptureActive() ?? false else { return }
        captureController?.changeCameraPosition(isFrontCamera: isFront)
    }
    
    private func createViews() {
        addSubview(previewView)
        tag = kViewDontHideTag

        localVideoGradient = CAGradientLayer()
        localVideoGradient.colors = [UIColor(white: 0, alpha: 0.8).cgColor ,UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor(white: 0, alpha: 0.8).cgColor]
        localVideoGradient.locations = [0,0.25,0.74,1]
        gradientView.layer.addSublayer(localVideoGradient)
        addSubview(gradientView)
        
        flipCameraButton = createButton(type: .plain,image: UIImage(named:"groupcall-flipcamera")!,
                                        action: #selector(userDidPressFlipCamera))
        
        flipCameraButton.accessibilityLabel = NSLocalizedString("CALL_VIEW_SWITCH_CAMERA_DIRECTION", comment: "Accessibility label to toggle front- vs. rear-facing camera")
        addSubview(flipCameraButton)
    }
    
    private func createViewsConstraints() {
        previewView.autoPinEdgesToSuperviewEdges()
        
        gradientView.autoPinEdgesToSuperviewEdges()
    
        flipCameraButton.autoPinEdge(toSuperviewEdge: .top, withInset: 5)
        flipCameraButton.autoPinEdge(toSuperviewEdge: .trailing, withInset: 5)
        flipCameraButton.autoSetDimension(.height, toSize: 30)
        flipCameraButton.autoSetDimension(.width, toSize: 30)
    }
    
    private func layoutGradient() {
        layoutIfNeeded()
        localVideoGradient.frame = self.gradientView.bounds
    }
    
    @objc func userDidPressFlipCamera(sender: UIButton) {
        AssertIsOnMainThread()
        sender.isSelected = !sender.isSelected
        
        let isUsingFrontCamera = !sender.isSelected
        changeCameraPosition(isUsingFrontCamera)
        Logger.info(" Camera is \(isUsingFrontCamera ? "front" : "back" )")
    }
}

// MARK: - Helper Method -
extension DSGroupCallLocalVideoView {
    private func createButton(type: DSVideoCallButtonType,image: UIImage, action: Selector) -> DSVideoCallButton {
        let button = DSVideoCallButton(type)
        button.setImage(image, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        return button
    }
}
