//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation
import UIKit
import CometChatPro

enum DSVideoCallButtonType {
    case dynamic
    case plain
    case system
}

enum DSVideoCallAudioSourceType {
    case speaker
    case earpiece
    case bluetooth
    case headset
}

enum DSVideoCallAvatarType {
    case local
    case remote
}

class DSVideoCallButton: UIButton {
    private var type: DSVideoCallButtonType
    private var colorBasic = UIColor(white: 1, alpha: 0.1)
    private var colorPressed = UIColor(white: 1, alpha: 0.5)
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted  && type == .dynamic {
                backgroundColor = colorPressed
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            switch type {
            case .dynamic:
                backgroundColor = self.isSelected ? colorPressed : colorBasic
            case .plain:
                backgroundColor = colorBasic
            case .system:
                print(" ")
            }
        }
    }

    init(_ type: DSVideoCallButtonType) {
        self.type = type
        super.init(frame: CGRect.zero)
        self.layer.cornerRadius = 10
        self.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.titleLabel?.numberOfLines = 2
        
        let size = buttonSize()
        self.autoSetDimensions(to: CGSize(width: size, height: size))
        self.titleEdgeInsets.top = size + 28
        
        if type != .system {
            backgroundColor = colorBasic
        }
    }
    
    public func setBasicColor(_ color: UIColor) {
        self.colorBasic = color
        backgroundColor = colorBasic
    }
    
    public func setCustomBackgroundImage(image: UIImage) {
        let bgView = UIImageView(image: image)
        self.addSubview(bgView)
        bgView.autoCenterInSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buttonSize() -> CGFloat {
//        return ScaleFromIPhone5To7Plus(55, 75)
        return 65
    }
    
    private func buttonInset() -> CGFloat {
        return 8
    }
}

class DSVideoCallAudioSourceButton: DSVideoCallButton {
    var buttonInputType: DSVideoCallAudioSourceType
    
    init() {
        buttonInputType = .headset
        super.init(.plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func switchImage(to type: DSVideoCallAudioSourceType) {
        guard buttonInputType != type else { return }
        buttonInputType = type
    
        switch type {
        case .bluetooth:
            self.setImage(UIImage(named:"groupcall-bluetooth")!, for: .normal)
        case .speaker:
            self.setImage(UIImage(named:"groupcall-speaker")!, for: .normal)
        case .earpiece,.headset:
            let earpiece = UIImage(named:"groupcall-earpiece")!
//                .asTintedImage(color: UIColor.white)
            self.setImage(earpiece, for: .normal)
        }
    }
}

let kGroupCallBackgroudAvatarSize: CGFloat = 100
let kGroupCallAvatarSize: CGFloat = 42
let kGroupCallMuteIconSize: CGFloat = 18
let kGroupCallCircleViewsAlpha: CGFloat = 0.6

class DSVideoCallAvatar: UIView {
    var number: String
    var avatarType: DSVideoCallAvatarType
    var imageView: UIImageView!
    
    var muteIcon: UIImageView = {
        let muteIcon = UIImageView(image: UIImage(named: "groupcall-mic")!)
//            .asTintedImage(color: .black))
        muteIcon.backgroundColor = .white
        muteIcon.layer.cornerRadius = kGroupCallMuteIconSize / 2
        muteIcon.isHidden = true
        return muteIcon
    }()
    
    init(number: String,type: DSVideoCallAvatarType ) {
        self.number = number
        self.avatarType = type
        super.init(frame: CGRect.zero)
        createImageView()
        buildAvatar()
        layoutViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createImageView() {
        imageView = UIImageView()
        imageView.layer.borderWidth = 2.0
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.masksToBounds = true
        imageView.alpha = kGroupCallCircleViewsAlpha
        if avatarType == .local {
            imageView.layer.cornerRadius = kGroupCallBackgroudAvatarSize / 2
        } else {
            imageView.layer.cornerRadius = kGroupCallAvatarSize / 2
        }
    }
    private func buildAvatar() {
        if avatarType == .local {
            guard let user = CometChat.getLoggedInUser() else { return }
            let avatar = Avatar(frame: CGRect(x: 0, y: 0, width: kGroupCallBackgroudAvatarSize, height: kGroupCallBackgroudAvatarSize))
            avatar.set(cornerRadius: 19).set(borderColor: .clear).set(backgroundColor: #colorLiteral(red: 0.6274509804, green: 0.7607843137, blue: 1, alpha: 1)).set(image:user.avatar ?? "" , with: user.name ?? "")
            imageView.addSubview(avatar)
        }
        if avatarType == .remote {
            var remoteUser: User?
            
            CometChat.getUser(UID: self.number, onSuccess: { (user) in
                guard let remoteUsers = user else { return }
                remoteUser = remoteUsers
            }) { (error) in
                Logger.error("Error while fetching user")
                return
            }
            
            DispatchQueue.main.async {
                let avatar = self.createAvatar(user: remoteUser, uid: self.number)
                self.imageView.addSubview(avatar)
            }
        }
    }
    
    private func createAvatar(user: User?, uid: String) -> UIImageView {
       let avatar = Avatar(frame: CGRect(x: 0, y: 0, width: kGroupCallAvatarSize, height: kGroupCallAvatarSize))
        let color = UIColor.random
        if user != nil {
            avatar.set(cornerRadius: 19).set(borderColor: .clear).set(backgroundColor: color).set(image:user!.avatar ?? "" , with: user!.name ?? "")
        } else  {
            avatar.set(cornerRadius: 19).set(borderColor: .clear).set(backgroundColor: color).set(image:"" , with: uid)
        }
        return avatar
    }
    
    
    public func getNumber() -> String {
        return number
    }
    
    public func mute(is state: Bool) {
        guard muteIcon.isHidden != state else { return }
        muteIcon.isHidden = state
    }
    
    private func layoutViews() {
        self.addSubview(imageView)
        imageView.autoPinEdgesToSuperviewEdges()
        imageView.autoSetDimensions(to: CGSize(width: kGroupCallAvatarSize, height: kGroupCallAvatarSize))
        
        self.addSubview(muteIcon)
        muteIcon.autoSetDimensions(to: CGSize(width: kGroupCallMuteIconSize, height: kGroupCallMuteIconSize))
        muteIcon.autoPinEdge(toSuperviewEdge: .leading)
        muteIcon.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
}

