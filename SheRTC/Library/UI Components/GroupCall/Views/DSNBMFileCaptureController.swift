//
//  NBMFileCaptureController.swift
//  openWebsocket newRTC
//
//  Created by Max on 10.01.2020.
//  Copyright © 2020 Max Mihailov. All rights reserved.
//

import Foundation
class DSNBMFileCaptureController {
    var capturer: RTCFileVideoCapturer
    
    init(capturer: RTCFileVideoCapturer) {
        self.capturer = capturer
    }
    
    func startCapture() {
        capturer.startCapturing(fromFileNamed: "foreman.mp4") { (error) in
            print("error while start capture")
        }
    }
    func stopCapture() {
        capturer.stopCapture()
    }
}
