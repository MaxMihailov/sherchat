
import UIKit

enum GroupCallCellDataPosition {
    case top
    case center
    case bottom
}

class DSGroupCallCollectionCell: UICollectionViewCell {
    //MARK: - Dependecies
//    private var contactsManager: OWSContactsManager = {
//        return Environment.shared.contactsManager
//    }()
    
    //MARK: - Vars
    private var dataPosition: GroupCallCellDataPosition!
    private var videoView: DSRemoteVideoView? = nil
    private var number: String? {
        didSet {
            if self.number != nil {
                updateProfileName()
            }
        }
    }
    
    private var localVideoGradient: CAGradientLayer?
    private var gradientView: UIView?
    
    private var videoLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("GROUPCALL_VIDEO_IS_OFF", comment: "Group call label showing that interlocutor disabled video")
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    private var muteIcon: UIImageView = {
        let icon = UIImageView(image: UIImage(named: "groupcall-mic"))
        icon.bounds = CGRect(x: 0, y: 0, width: 26, height: 26)
        icon.isHidden = true
        return icon
    }()
    
    private var nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.textAlignment = .center
        return lbl
    }()
    
    //MARK: - Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = UIColor.black
    }
     
    required init?(coder: NSCoder) {
        fatalError("init(coder) is not implemented")
    }
    
    public func setVideoView(to view: DSRemoteVideoView?,number: String,center: CGPoint?) {
        if view != nil {
            self.videoView = view
            setNeedsLayout()
        }
        self.dataPosition = findCellDataPosition(center: center)
        self.number = number
        createGradient()
        layoutViews()
    }
    
    private func findCellDataPosition(center: CGPoint?) -> GroupCallCellDataPosition {
        guard let centerPoint = center else { return .bottom }
        let fullCenter = UIScreen.main.bounds.center
        if fullCenter == center {
            return .center
        }
        let fullHeight = UIScreen.main.bounds.height
        let cellPoint = centerPoint.y
        
        if cellPoint < (fullHeight / 2) {
            return .bottom
        } else {
            return .top
        }
    }
    
    public func soundIsActive(_ active: Bool) {
        muteIcon.isHidden = active
    }
    
    private func createGradient() {
        guard dataPosition != .center else { return }
        let clearColor = UIColor.clear.cgColor
        let blackColor = UIColor(white: 0, alpha: 0.6).cgColor
        
        var colors: [Any]?
        switch dataPosition {
        case .bottom:
           colors = [clearColor,clearColor,
            clearColor,blackColor]
            break
        case .top:
            colors = [blackColor,clearColor,
            clearColor,clearColor]
            break
        default:
            ()
        }
        
        gradientView = UIView()
        localVideoGradient = CAGradientLayer()
        localVideoGradient!.colors = colors
        localVideoGradient!.locations = [0,0.25,0.74,1]
        localVideoGradient?.frame = self.bounds
        gradientView!.layer.addSublayer(localVideoGradient!)
    }
    
    private func updateProfileName() {
//        let address = SignalServiceAddress(phoneNumber: self.number!)
        self.nameLabel.text = self.number!
    }
    
    private func layoutViews() {
        contentView.addSubview(videoView!)
        videoView?.autoPinEdgesToSuperviewEdges()
        showCellSubview(videoView!)
        
        contentView.addSubview(nameLabel)
        if dataPosition! == .top || dataPosition! == .center{
            nameLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 20)
        } else {
            nameLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 20)
        }
        
        nameLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: 16)
        
        contentView.addSubview(muteIcon)
        muteIcon.autoAlignAxis(.horizontal, toSameAxisOf: nameLabel)
        muteIcon.autoPinEdge(.leading, to: .trailing, of: nameLabel, withOffset: 5)
        
        if gradientView != nil {
            contentView.addSubview(gradientView!)
            gradientView?.autoPinEdgesToSuperviewEdges()
        }
        
        contentView.bringSubviewToFront(nameLabel)
    }
    
    // MARK: - Helpers methods
    public func getNumber() -> String? {
        return number
    }
    
    private func showCellSubview(_ view: UIView) {
        view.transform = CGAffineTransform(scaleX: 0.01,y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            view.transform = CGAffineTransform.identity
        },completion: nil)
    }
    
    private func hideCellSubview(_ view: UIView) {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            view.transform = CGAffineTransform(scaleX: 0.01, y: 0.01);
        }, completion: nil)
    }
    
    private func getGradientColors(center: CGPoint) -> [Any]? {
        let clearColor = UIColor.clear.cgColor
        let blackColor = UIColor(white: 0, alpha: 0.6).cgColor
        let fullCenter = UIScreen.main.bounds.center
        if fullCenter == center {
            return nil
        }
        let fullHeight = UIScreen.main.bounds.height
        let cellPoint = center.y
        
        if cellPoint < (fullHeight / 2) {
            return [clearColor,clearColor,
                    clearColor,blackColor]
        } else {
            return [blackColor,clearColor,
                    clearColor,clearColor]
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
//        self.removeAllConstraints()
        
        number = nil
        videoView = nil
        nameLabel.text = ""
        muteIcon.isHidden = true
        gradientView?.removeFromSuperview()
        gradientView = nil
        localVideoGradient = nil
        
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
    }
}
