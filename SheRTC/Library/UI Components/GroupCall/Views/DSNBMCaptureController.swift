//
//  NBMCaptureController.swift
//  openWebsocket newRTC
//
//  Created by Max on 09.01.2020.
//  Copyright © 2020 Max Mihailov. All rights reserved.
//

import Foundation
typealias Completition = ((CGSize?) -> Void)

class DSNBMCaptureController {
    let capturer: RTCCameraVideoCapturer
    var usingFrontCamera: Bool
    var isActive: Bool
    
    init(capturer: RTCCameraVideoCapturer,usingFrontCamera: Bool) {
        self.capturer = capturer
        self.usingFrontCamera = usingFrontCamera
        self.isActive = false
    }
    
    /// Starting capture using format
    ///
    /// - Returns: resolution of used format
    public func startCapture(completition: @escaping Completition) {
        let position = usingFrontCamera ? AVCaptureDevice.Position.front : AVCaptureDevice.Position.back
        let device = findDeviceForPosition(position)
        let format = selectFormatForDevice(device)
        guard format != nil else { completition(nil); return}
        
        capturer.startCapture(with: device, format: format!, fps: 60) { error in
            self.isActive = true
            let videoRes =  CMVideoFormatDescriptionGetDimensions(format!.formatDescription)
            completition(CGSize(width: Int(videoRes.width), height: Int(videoRes.height)))
        }
    }
    public func stopCapture() {
        self.isActive = false
        capturer.stopCapture()
    }
    
    public func changeCameraPosition(isFrontCamera: Bool) {
        self.usingFrontCamera = isFrontCamera
        startCapture { (_) in }
    }
    
    public func isCaptureActive() -> Bool {
        return isActive
    }
    
    private func findDeviceForPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice {
        let captureDevices = RTCCameraVideoCapturer.captureDevices()
        for device in captureDevices {
            if device.position == position {
                return device
            }
        }
        return captureDevices[0]
    }
    
   private func selectFormatForDevice(_ device: AVCaptureDevice) -> AVCaptureDevice.Format? {
        let formats = RTCCameraVideoCapturer.supportedFormats(for: device)
        let targetWidth: Int32 = kGroupCallVideoWidth
        let targetHeight: Int32 = kGroupCallVideoHeight
        var currentDiff = INT_MAX
        
        var selectedFormat: AVCaptureDevice.Format?
        for format in formats {
            let dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription)
            let pixelFormat = CMFormatDescriptionGetMediaSubType(format.formatDescription)
            let diff = abs(targetWidth - dimension.width) + abs(targetHeight - dimension.height)
            
            if diff < currentDiff {
                selectedFormat = format
                currentDiff = diff
            } else if (diff == currentDiff && pixelFormat == capturer.preferredOutputPixelFormat()) {
                selectedFormat = format
            }
        }
        return selectedFormat
    }
}
