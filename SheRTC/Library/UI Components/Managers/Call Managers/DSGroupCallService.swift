//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation
import CometChatPro

public protocol DSGroupCallServiceDelegate: class {
    func didChangeStreamsList(remoteStreams: [String: RTCMediaStream])
    func didAddLocalCameraCapturer(cameraVideoCapturer: RTCCameraVideoCapturer)
    func didAddLocalFileCapturer(capturer: RTCFileVideoCapturer)
    func handleRemoteAudioState(is active: Bool,number: String)
    func handleRemoteVideoState(is active: Bool,number: String)
    func didFailWithError(error: String)
    func iceStatusChanged(state: RTCIceConnectionState,number: String)
    func resetUIState()
    func endCallView()
}

public let kGroupCallVideoWidth: Int32 = 640
public let kGroupCallVideoHeight: Int32 = 480
public let kVideoCallFPS: Int32 = 26

public class DSGroupCallService: NSObject {
    
    let callCommandsManager: DSCallCommandsManager
    
    // MARK: - Properties
    var observers = [Weak<CallServiceObserver>]()
    
    public var callUIAdapter: DSCallUIAdapter!
    
    //MARK: Delegate
    weak var delegate: DSGroupCallServiceDelegate? {
        didSet{
            delegate?.didChangeStreamsList(remoteStreams: self.remoteStreams)
            if cameraCapturer != nil {
                delegate?.didAddLocalCameraCapturer(cameraVideoCapturer: self.cameraCapturer!)
            }
            if fileCapturer != nil {
                delegate?.didAddLocalFileCapturer(capturer: self.fileCapturer!)
            }
        }
    }
    
    //MARK: Constants
    private var userName: String = "UnregisteredUser"
    
    //MARK: - Kurento API
    private var _roomEntity: DSRoomEntity?
    public var roomEntity: DSRoomEntity? {
        set {
            AssertIsOnMainThread()
            
            let oldValue = _roomEntity
            _roomEntity = newValue
            
            oldValue?.call?.removeObserver(self)
            newValue?.call?.addObserverAndSyncState(observer: self)
            
            
            for observer in observers {
                observer.value?.didUpdateCall(call: newValue?.call)
            }
        }
        get {
            return _roomEntity
        }
    }
    
    private var room: NBMRoom?
    private var roomClient: NBMRoomClient?
    private var webRTCPeer: NBMWebRTCPeer?
    private let mediaConfiguration = NBMMediaConfiguration.default()!
    internal var cameraCapturer: RTCCameraVideoCapturer?
    internal var fileCapturer: RTCFileVideoCapturer?
    
    //MARK: - Variables
    public var isActiveCall = false
    private var retryCount = 0
    private var busyMembersSet = Set<String>()
    private var timer: Timer?
    private var lastAudioState: Bool?
    private var lastVideoState: Bool?
    // MARK: - Persistence
    private  var remoteStreams = [String: RTCMediaStream]() {
        willSet {
            delegate?.didChangeStreamsList(remoteStreams: newValue)
        }
    }
    
    //MARK: - Calculated Variables
    private var localPeer: NBMPeer? {
        return roomClient?.room.localPeer
    }
    
    private var joined: Bool? {
        get {
            return roomClient?.isJoined
        }
    }
    
    //MARK: - Service Methods -
    @objc public override init() {
        callCommandsManager = DSCallCommandsManager()
        super.init()
        SwiftSingletons.register(self)
//        CometChat.getLoggedInUser()?.uid 
        let initiateCallNotifyName = Notification.Name(Notification_IncomingCall)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(initiateCall(_:)),
                                               name: initiateCallNotifyName,
                                               object: nil)
        
        let busyNotifyName = Notification.Name(Notification_RecipientBusy)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(busyCommand(_:)),
                                               name: busyNotifyName,
                                               object: nil)
        
        let unlinkNotifyName = Notification.Name(Notification_UnlinkVideoconferencing)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(unlinkCommand(_:)),
                                               name: unlinkNotifyName,
                                               object: nil)
    }
    
    @objc
    public func createUIAdapter() {
        AssertIsOnMainThread()
        self.callUIAdapter = DSCallUIAdapter(callService: self)
    }
    
    @objc
    func roomNameIfCallActive() -> NSString { // TODO: Delete when be sure
        if isActiveCall, roomEntity != nil {
            return roomEntity!.nameRoom as NSString
        }
        return ""
    }

    // MARK: - Observers
    // The observer-related methods should be invoked on the main thread.
    func addObserverAndSyncState(observer: CallServiceObserver) {
        AssertIsOnMainThread()
        
        observers.append(Weak(value: observer))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Initialize Call -
extension DSGroupCallService {
    public func initializeManager(with roomEntity: DSRoomEntity) {
        self.roomEntity = roomEntity
        self.busyMembersSet = Set(roomEntity.listParticipants)
        
        if isActiveCall, roomClient?.room.name == roomEntity.nameRoom {
            Logger.info("Continue last call")
        } else {
            Logger.info("Starting new call!")
            initializeNewConnection()
        }
        Logger.info("isFirstCaller = \(roomEntity.isFirstCaller)")
    }
    
    private func initializeNewConnection() {
        initializeRTCPeer()
        initializeRoomClient()
        initializeReach()
        initializeTimer()
    }
    
    private func initializeRoomClient() {
        guard let roomEntity = roomEntity else { return }
        guard let userName = updateUserName() else {
            delegate?.didFailWithError(error: "Can't find local number")
            localHungupCall()
            return
        }
        self.userName = userName
        let kurentoServer = URL(string: "https://im-test.dstarlab.com:8443/room")!
        let roomInit = NBMRoom(username: userName, roomName: roomEntity.nameRoom, roomURL: kurentoServer, dataChannels: true)
        
        guard let room = roomInit else {
            owsFailDebug("NBMRoom is unexpectedly nil")
            return
        }
        self.room = room
        
        roomClient = NBMRoomClient(room: room, timeout: 0, delegate: self)
        assert(roomClient != nil)
    }
    
    private func initializeReach() {
        guard let roomClient = roomClient else { return }
        Logger.debug("Initializing Reachability")
        Logger.debug("Connection state is \(roomClient.connectionState) \n")
        if roomClient.connectionState == NBMRoomClientConnectionState.closed {
            manageRoomClientConnection()
        }
    }
    
    private func initializeRTCPeer() {
        Logger.debug("Initializing RTCPeer")
        mediaConfiguration.videoBandwidth = 300000
        mediaConfiguration.audioBandwidth = 300000
        let cmDimensions = CMVideoDimensions(width: kGroupCallVideoWidth, height: kGroupCallVideoHeight)
        mediaConfiguration.receiverVideoFormat = NBMVideoFormat(dimensions: cmDimensions, pixelFormat: .format420f, frameRate: Double(kVideoCallFPS))
        
        let rtcPeer = NBMWebRTCPeer(delegate: self, configuration: mediaConfiguration)
        guard let webRTCPeer = rtcPeer else {
            Logger.error("WebRTCPeer is nil!")
            return
        }
        
        self.webRTCPeer = webRTCPeer
        webRTCPeer.startLocalMedia()
    }
    
    private func initializeTimer() {
        Logger.info("Initializing Timer")
        invalidateAndDeleteTimerIfNeeded()
        timer = Timer(timeInterval: 30, target: self, selector: #selector(callTimerDidFinish), userInfo: nil, repeats: false) //FIXME: Change interval after testing
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    private func updateUserName() -> String? {
        guard let number = CometChat.getLoggedInUser()?.uid else { return  nil }
        let timestamp = String(Date().timeIntervalSince1970)
        Logger.info("Kurento userName \(number)")
        return number + ":" + timestamp
    }
}

// MARK: - NBMRoomClientDelegate -
extension DSGroupCallService: NBMRoomClientDelegate {
    public func client(_ client: NBMRoomClient!, isConnected connected: Bool) {
        switch connected {
        case true:
            Logger.debug("Connected to Room!")
            if joined == false {
                roomClient?.joinRoom(withDataChannels: true)
            }
        case false:
            Logger.debug("Connected is False")
            manageRoomClientConnection()
        }
    }
    
    public func client(_ client: NBMRoomClient!, didJoinRoom error: Error!) {
        if error != nil {
            let nserror = error as NSError
            Logger.error("Error joining Room:  \(nserror.code),\(nserror.localizedDescription)")
            
            if nserror.code != 803 { // Error "Invalid method lacking parameter stream" is`t fatal, so continue
                manageJoiningRoom()
                return
            }
//            roomEntity?.nameRoom += "803"
//            safeRestartCall()
            delegate?.didFailWithError(error: "Error while connecting to room. Transfered to another room.")
        }
        Logger.debug("Successfully joined to Room!")
        
        isActiveCall = true
        
        if roomEntity != nil, roomEntity!.isFirstCaller {
            callCommandsManager.sendCallCommand(roomEntity!)
            roomEntity!.isFirstCaller = false
            Logger.debug("Sending call commands")
        }
        generateOfferForAllPeers()
    }
    
    public func client(_ client: NBMRoomClient!, participantJoined peer: NBMPeer!) {
    }
    
    public func client(_ client: NBMRoomClient!, participantLeft peer: NBMPeer!) {
        let connectionId = getIdFromPeer(peer)
        webRTCPeer?.closeConnection(withConnectionId: connectionId)
    }
    
    public func client(_ client: NBMRoomClient!, participantPublished peer: NBMPeer!) {
        let peerConnection = connectionOfPeer(peer)
        if peerConnection == nil && peer.streams.count > 0 {
            generateOffer(for: peer)
        }
    }
    
    public func client(_ client: NBMRoomClient!, participantUnpublished peer: NBMPeer!) {
        let connectionId = getIdFromPeer(peer)
        webRTCPeer?.closeConnection(withConnectionId: connectionId)
    }
    
    public func client(_ client: NBMRoomClient!, didReceive candidate: RTCIceCandidate!, fromParticipant peer: NBMPeer!) {
        let connectionId = getIdFromPeer(peer)
        webRTCPeer?.add(candidate, connectionId: connectionId)
    }
    
    public func client(_ client: NBMRoomClient!, didFailWithError error: Error!) {
        Logger.error("Did Fail With Error \(error.localizedDescription)")
        delegate?.didFailWithError(error: error.localizedDescription)
        localHungupCall()
    }
    
    public func client(_ client: NBMRoomClient!, didLeaveRoom error: Error!) {
        Logger.debug("Did leave room")
        if roomClient?.isConnected ?? false {
            roomClient?.disconnect()
        }
        roomClient = nil
    }
    
    // - RoomClient messaging methods -
    public func client(_ client: NBMRoomClient!, didReceiveMessage message: String!, fromParticipant peer: NBMPeer!) {
        let peerNumber = getIdFromPeer(peer).getNumber()
        let userNumber = userName.getNumber()
        
        if message.getNumber() == userNumber { // TODO: Delete when resolved problem with remoteStream tracks info
            if message.contains(kMessageAllMuted) {
                parseAllAudioMessage(text: message, video: false)
                return
            }
            if message.contains(kMessageAllNoVideo) {
                parseAllVideoMessage(text: message, video: true)
                return
            }
        }
        
        if message.contains(userNumber) {
            if message.contains(kMessageHideVideo) {
                delegate?.handleRemoteVideoState(is: false, number: peerNumber)
            }
            if message.contains(kMessageShowVideo) {
                delegate?.handleRemoteVideoState(is: true, number: peerNumber)
            }
            if message.contains(kMessageMicIsOff) {
                delegate?.handleRemoteAudioState(is: false, number: peerNumber)
            }
            if message.contains(kMessageMicIsOn) {
                delegate?.handleRemoteAudioState(is: true, number: peerNumber)
            }
            if message.contains(kMessageUpdateLocalMedia) {
                updateLocalMedia()
            }
        }
    }
    
    /// Sending info messages via roomClient
    ///
    /// - Parameters:
    ///   - enable: Bool
    ///   - mode: 0 - audio, 1 - video
    private func sendVideoAudioModeMessage(_ enable: Bool,mode: Int) {
        let localNumber = CometChat.getLoggedInUser()?.uid ?? ""
        let numbers = [String](remoteStreams.keys).filter { $0 != localNumber }.joined(separator: ",")
        var message = "\(numbers): "
        
        if enable {
            switch mode {
            case 0:  message.append(kMessageMicIsOn)
            case 1:  message.append(kMessageShowVideo)
            default: ()
            }
        } else {
            switch mode {
            case 0:  message.append(kMessageMicIsOff)
            case 1:  message.append(kMessageHideVideo)
            default: ()
            }
        }
        roomClient?.sendMessage(message)
    }
    
    private func sendAllAudioVideoInfoToNewMember(number: String) {
        guard let roomEntity = roomEntity else { return }
        DispatchQueue.global().asyncAfter(deadline: .now() + 2.5, execute: { [weak self] in // wait 2.5 sec to be sure that
            guard let self = self else { return }                                           // receiver is already connected
            if roomEntity.noAudioList.count > 0 {
                let allAudioMessage = "\(number):\(kMessageAllMuted)-" + roomEntity.noAudioList.joined(separator: ",")
                self.roomClient?.sendMessage(allAudioMessage)
            }
            if roomEntity.noVideoList.count > 0 {
                let allVideoMessage =  "\(number):\(kMessageAllNoVideo)-" + roomEntity.noVideoList.joined(separator: ",")
                self.roomClient?.sendMessage(allVideoMessage)
            }
        })
    }
    
    private func parseAllVideoMessage(text: String,video:Bool) {
        var noVideoNumbers = ""
        if let index = text.range(of: "-")?.upperBound {
            noVideoNumbers = String(text[index...])
        }
        for num in noVideoNumbers.split(separator: ",") {
            delegate?.handleRemoteVideoState(is: false, number: String(num))
        }
    }
    
    private func parseAllAudioMessage(text: String,video:Bool) {
        var noAudioNumbers = ""
        if let index = text.range(of: "-")?.upperBound {
            noAudioNumbers = String(text[index...])
        }
        for num in noAudioNumbers.split(separator: ",") {
            delegate?.handleRemoteAudioState(is: false, number: String(num))
        }
    }
    
    // - RoomClient connection manage methods -
    private func manageJoiningRoom() {
        Logger.info("Managing Client Join Room")
        let canRetry = retryCount < 5
        
        if canRetry {
            roomClient?.leaveRoom({ (error) in
                if error != nil {
                    self.roomClient?.joinRoom()
                }
            })
            Logger.debug("Retry joining to room \(retryCount)")
            retryCount += 1
        } else {
            Logger.error("Teardown service due join room fail")
            let joinProblemsString = NSLocalizedString("JOIN_PROBLEMS_GROUP_CALL", comment: "")
            delegate?.didFailWithError(error: joinProblemsString)
            
            localHungupCall()
        }
    }
    
    private func manageRoomClientConnection() {
//        Logger.info("Managing Room Client Connection")
//        let canRetry = retryCount < 3
//        let isReachable = reachabilityManager.isReachable
//
//        if canRetry && isReachable  {
//            Logger.debug("Retry connecting to room")
//            retryCount += 1
//            roomClient?.connect()
//        } else {
//            if reachabilityManager.isReachable == false {
//                NotificationCenter.default.addObserver(self,
//                                                       selector: #selector(waitForConnection),
//                                                       name: NSNotification.Name.reachabilityChanged,
//                                                       object: nil)
//                return
//            }
//
//            Logger.error("Teardown service due join room fail")
//            let connectionProblemsString = NSLocalizedString("CONNECTION_PROBLEMS_GROUP_CALL", comment: "")
//            delegate?.didFailWithError(error: connectionProblemsString)
//
//            localHungupCall()
//        }
    }
    
    @objc
    private func waitForConnection() {
//        guard delegate != nil else {
//            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
//            return
//        }
//        if self.reachabilityManager.isReachable {
//            Logger.debug("isReachable: true")
//            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
//
//            safeRestartCall()
//        } else {
//            Logger.debug("isReachable: false")
//        }
    }
    
    private func safeRestartCall() {
        guard let roomEntity = roomEntity else {
            Logger.error("Cannot restart call")
            localHungupCall()
            return
        }
        
        Logger.debug("Restarting active call")
        self.deinitializeRoomManager()
        roomEntity.call.didCallTerminate = false
        self.initializeManager(with: roomEntity)
        delegate?.resetUIState()
    }
}

// MARK: - NBMWebRTCPeerDelegate -
extension DSGroupCallService: NBMWebRTCPeerDelegate {
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didAdd remoteStream: RTCMediaStream!, of connection: NBMPeerConnection!) {
        let number = connection.connectionId.getNumber()
        if connection.connectionId != userName {
            Logger.debug("didAddRemoteStream for connection \(number)")
            invalidateAndDeleteTimerIfNeeded()
            
            roomEntity?.removeAudioVideoStates(for: number)
            // If new member added to group and connecting to call, we have to update room entity
//            roomEntity?.addToParticipantsIfNeeded(number: number)
            
            if remoteStreams.keys.contains(where: {$0 == number}) {
                remoteStreams.removeValue(forKey: number)
            }
            remoteStreams[number] = remoteStream
        }
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didRemove remoteStream: RTCMediaStream!, of connection: NBMPeerConnection!) {
        Logger.debug("didRemoveRemoteStream for \(connection.connectionId ?? "error")")
        guard let connectionId = connection.connectionId else { return }
        let remoteIdentifiers = remotePeers()?.compactMap { $0.identifier }
        let number = connectionId.getNumber()
        
        if remoteIdentifiers?.contains(connectionId) ?? false {
            roomEntity?.removeAudioVideoStates(for: number)
            remoteStreams.removeValue(forKey: number)
        }
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didAdd dataChannel: RTCDataChannel!) {
        Logger.info("didAddDataChannel")
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didCreateVideoSource videoSource: RTCVideoSource!) {
        videoSource.adaptOutputFormat(toWidth: kGroupCallVideoWidth, height: kGroupCallVideoHeight, fps: kVideoCallFPS)
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didCreateLocalCapturer cameraCapturer: RTCCameraVideoCapturer!) {
        self.cameraCapturer = cameraCapturer
        delegate?.didAddLocalCameraCapturer(cameraVideoCapturer: cameraCapturer)
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didCreateLocalFileCapturer fileCapturer: RTCFileVideoCapturer!) {
        self.fileCapturer = fileCapturer
        delegate?.didAddLocalFileCapturer(capturer: fileCapturer)
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didGenerateOffer sdpOffer: RTCSessionDescription!, for connection: NBMPeerConnection!) {
        Logger.info("didGenerateOffer \(String(describing: connection.connectionId))")
        if connection.connectionId == userName {
            Logger.debug("Video published")
            roomClient?.publishVideo(sdpOffer.sdp, loopback: false) { sdpAnswer,error in
                self.webRTCPeer?.processAnswer(sdpAnswer, connectionId: connection.connectionId)
            }
        } else {
            let remotePeer = peerOfConnection(connection)
            roomClient?.receiveVideo(from: remotePeer, offer: sdpOffer.sdp) { (sdpAnswer, error) in
                self.webRTCPeer?.processAnswer(sdpAnswer, connectionId: connection.connectionId)
            }
        }
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, didGenerateAnswer sdpAnswer: RTCSessionDescription!, for connection: NBMPeerConnection!) {
        Logger.info("didGenerateAnswer")
    }
    
    public func webRTCPeer(_ peer: NBMWebRTCPeer!, hasICECandidate candidate: RTCIceCandidate!, for connection: NBMPeerConnection!) {
//        Logger.info("hasICECandidate \(candidate.description)")
        guard let remotePeer = peerOfConnection(connection) else { return }
        if remotePeer.identifier == nil {
            roomClient?.send(candidate, for: localPeer)
        } else {
            roomClient?.send(candidate, for: remotePeer)
        }
    }
    
    public func webrtcPeer(_ peer: NBMWebRTCPeer!, iceStatusChanged state: RTCIceConnectionState, of connection: NBMPeerConnection!) {
        switch state {
        case RTCIceConnectionState.connected:
            Logger.info("Ice Status Connected \(String(describing: connection.connectionId))")
            // sending call state on ice connected
            let phoneNumberOfConnection = connection.connectionId.getNumber()
            if remoteStreams.keys.contains(phoneNumberOfConnection) {
                Logger.info("Call member state sended to \(phoneNumberOfConnection)")
                sendCallStateMessageIfNedeed(to: phoneNumberOfConnection)
                
                if lastAudioState != nil || lastVideoState != nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
                        self?.sendLatestLocalAudioVideoInfo()
                    })
                }
            }
            
            delegate?.iceStatusChanged(state: state, number: connection.connectionId)
            break
        case RTCIceConnectionState.closed:
            Logger.info("Ice Status Closed \(String(describing: connection.connectionId))")
            webRTCPeer?.closeConnection(withConnectionId: connection.connectionId)
        case RTCIceConnectionState.disconnected:
            break
        case RTCIceConnectionState.failed:
            Logger.error("Ice Status Failed \(String(describing: connection.connectionId))")
            webRTCPeer?.closeConnection(withConnectionId: connection.connectionId)
            break
        default:
            print("")
        }
    }
     
    // - sdp offers generating methods -
    private func generateOffer(for peer: NBMPeer) {
        Logger.debug("Generating Offer for \(String(describing: peer.identifier))")
        let connectionId = getIdFromPeer(peer)
        webRTCPeer?.generateOffer(connectionId, withDataChannels: true)
    }
    
    private func generateOfferForAllPeers() {
        guard let selfPeer = localPeer else { return }
        generateOffer(for: selfPeer)
        
        guard let remotePeers = remotePeers() else { return }
        for peer in remotePeers {
            guard peer.identifier.getNumber() != userName.getNumber() else { continue } // do not send offer to self with another timestamp
            let peerConnection = connectionOfPeer(peer)
            if peerConnection == nil && peer.streams.count > 0 {
                Logger.debug("Offer for \(String(describing: peer.identifier))")
                generateOffer(for: peer)
            }
        }
    }

    // - WebRTCPeer media methods -
    public func enableAudio(enable: Bool) {
        webRTCPeer?.enableAudio(enable)
        sendVideoAudioModeMessage(enable,mode: 0)
        if roomEntity != nil,
            roomEntity!.call.state != .connected  {
            lastAudioState = enable
        }
    }
    
    public func enableVideo(enable: Bool) {
        webRTCPeer?.enableVideo(enable)
        sendVideoAudioModeMessage(enable,mode: 1)
        if roomEntity != nil,
            roomEntity!.call.state != .connected  {
            lastVideoState = enable
        }
    }
    
    private func updateLocalMedia() {
        //check result of this method
        guard let rtcPeer = webRTCPeer else { return }
        let audioState = rtcPeer.isAudioEnabled()
        let videoState = rtcPeer.isVideoEnabled()
        
        rtcPeer.enableVideo(!audioState)
        rtcPeer.enableAudio(!videoState)
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 0.5) {
            rtcPeer.enableAudio(audioState)
            rtcPeer.enableVideo(videoState)
        }
    }
    
    private func sendLatestLocalAudioVideoInfo() {
        if lastAudioState != nil {
            enableAudio(enable: lastAudioState!)
            lastAudioState = nil
        }
        if lastVideoState != nil {
            enableVideo(enable: lastVideoState!)
            lastVideoState = nil
        }
    }
}

// MARK: - Deinitialize Call -
extension DSGroupCallService {
    
    @objc
    private func callTimerDidFinish() {
        Logger.debug("Timer did finish")
        invalidateAndDeleteTimerIfNeeded()
        localHungupCall()
        guard roomEntity != nil else { return }
        callCommandsManager.sendUnlinkCommand(roomEntity!)
    }
    
    private func invalidateAndDeleteTimerIfNeeded() {
        guard timer != nil else { return }
        Logger.debug("Timer invalidated")
        timer?.invalidate()
        timer = nil
    }
    
    private func localHungupCall() {
        guard let roomEntity = roomEntity else { return }
        callUIAdapter.stopOutgoingCall(roomEntity)
        endCallView()
    }
    
    private func endCallView() {
        delegate?.endCallView()
    }
    
    public func deinitializeRoomManager() {
        Logger.debug("Deinitialize Room Manager")
        busyMembersSet.removeAll()
        
        invalidateAndDeleteTimerIfNeeded()
        teardownRoomClient()
        teardownWebRTCPeer()
        
        isActiveCall = false
        
        guard roomEntity != nil, let call = roomEntity!.call else { return }
        switch call.direction {
        case .incoming:
            if call.state == .localRinging {
//                callCommandsManager.sendBusyCommand(roomEntity!)
            }
        case .outgoing:
            if call.state == .idle {
                callCommandsManager.sendUnlinkCommand(roomEntity!)
            }
        }
        roomEntity?.call.terminate()
        roomEntity = nil
    }
    
    private func teardownRoomClient() {
        room = nil
        roomEntity?.noAudioList.removeAll()
        roomEntity?.noVideoList.removeAll()
        if roomClient != nil && roomClient?.connectionState == .open {
            roomClient?.unpublishVideo()
            if roomClient!.isJoined {
                roomClient?.leaveRoom()
            }
        }
        retryCount = 0
    }
    
    private func teardownWebRTCPeer() {
        remoteStreams.removeAll()
        cameraCapturer = nil
        fileCapturer = nil
        lastAudioState = nil
        lastVideoState = nil
        webRTCPeer?.stopLocalMedia()
        webRTCPeer = nil
    }
}

// MARK: - Call Commands Handling -
extension DSGroupCallService {
    @objc
    func initiateCall(_ notification: Notification) {
        let body = notification.object as! String
        guard let index = body.range(of: DSCallCommands.startCall.rawValue)?.upperBound else { return }
        let rawBody = String(body[index...])
        callCommandsManager.initiateCall(rawBody, isActiveCall: isActiveCall) { (roomEntity) in
            guard let roomEntity = roomEntity else { return }
            self.roomEntity = roomEntity
        }
    }
    
    @objc
    private func busyCommand(_ notification: Notification) {
        guard roomEntity != nil,delegate != nil else {
            Logger.debug("Ingoring busy command")
            return
        }
        guard
            let userUid = DSCallCommandsManager.getDataFromNotification(notification,
                                                                       withCommand: DSCallCommands.busyCall.rawValue) else { return }
        if busyMembersSet.contains(userUid) {
            busyMembersSet.remove(userUid)
        }
//
        if busyMembersSet.count == 1 {
            Logger.info("closing the current call because no members")
            delegate?.didFailWithError(error: NSLocalizedString("END_CALL_RESPONDER_IS_BUSY", comment: ""))
            localHungupCall()
        }
        Logger.debug("Busy command executed")
    }
    
    @objc
    private func unlinkCommand(_ notification: Notification) {
        guard roomEntity != nil ,roomClient == nil else { // execute unlink command only if ringing
            Logger.debug("Ingoring unlink command")
            return
        }
        callCommandsManager.unlinkCommand(roomEntity!, notification)
    }
}

extension DSGroupCallService: CallObserver {
    public func stateDidChange(call: CallModel, state: CallState) {
        switch state {
        case .connected:
            ensureAudioState(call: call)
        default:
            ()
        }
    }
    
    public func hasLocalVideoDidChange(call: CallModel, hasLocalVideo: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    public func muteDidChange(call: CallModel, isMuted: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    public func holdDidChange(call: CallModel, isOnHold: Bool) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    public func audioSourceDidChange(call: CallModel, audioSource: AudioSource?) {
        AssertIsOnMainThread()
        
        Logger.debug("is no-op")
    }
    
    private func ensureAudioState(call: CallModel) {
        guard call.state == .connected else {
            webRTCPeer?.enableAudio(false)
            return
        }
        webRTCPeer?.enableAudio(true)
    }
}

// MARK: - Helpers Methods -
extension DSGroupCallService {
    
    private func getIdFromPeer(_ peer: NBMPeer) -> String {
        return peer.identifier
    }
    
    private func connectionOfPeer(_ peer: NBMPeer) -> NBMPeerConnection? {
        let peerId = getIdFromPeer(peer)
        return webRTCPeer?.connection(withConnectionId: peerId)
    }
    
    private func remotePeers() -> [NBMPeer]? {
        let peers = roomClient?.peers
        let remotePeers = peers?.compactMap({ $0 as? NBMPeer })
        return remotePeers
    }
    
    private func getAllPeers() -> Set<NBMPeer> {
        var set = Set<NBMPeer>()
        guard let selfPeer = localPeer else {
            return set
        }
        guard let remotePeers = remotePeers() else {
            set.insert(selfPeer)
            return set
        }
        for peer in remotePeers {
            set.insert(peer)
        }
        set.insert(selfPeer)
        return set
    }
    
    private func peerOfConnection(_ connection: NBMPeerConnection) -> NBMPeer? {
        let connectionId = connection.connectionId
        let allPeers = getAllPeers()
        let peer = allPeers.first(where: {connectionId == getIdFromPeer($0)})
        return peer
    }
    
    // When new user connects to call
    // one call member have to send info about no audio/no video members
    private func sendCallStateMessageIfNedeed(to number: String) {
        if shouldSendCallMembersState(to: number) {
            Logger.debug("Sending call state message to \(number)")
            sendAllAudioVideoInfoToNewMember(number: number)
        }
    }
    
    private func shouldSendCallMembersState(to number: String) -> Bool {
        let localNumber = userName.getNumber()
        let allPeers = getAllPeers().map { $0.identifier.getNumber() }
        if allPeers.count == 1 {
            return true
        }
        
        let firstSorted = allPeers.sorted().first(where: { $0 != number })
        if firstSorted != nil {
            return firstSorted == localNumber
        } else {
            return true
        }
    }
}

private extension String {
    
    func getNumber() -> String {
        var num = ""
        if let index = self.range(of: ":")?.lowerBound {
            num = String(self[..<index])
        }
        return num
    }
    
    func getTimestamp() -> String {
        var num = ""
        if let index = self.range(of: ":")?.upperBound {
            num = String(self[index...])
        }
        return num
    }
}
