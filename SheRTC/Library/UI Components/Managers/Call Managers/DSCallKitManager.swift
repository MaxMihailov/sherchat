//
//  Copyright (c) 2019 Open Whisper Systems. All rights reserved.
//

import UIKit
import CallKit


/**
 * Requests actions from CallKit
 *
 * @Discussion:
 *   Based on SpeakerboxCallManager, from the Apple CallKit Example app. Though, it's responsibilities are mostly
 *   mirrored (and delegated from) CallKitCallUIAdaptee.
 */
@available(iOS 10.0, *)
final class DSCallKitCallManager: NSObject {
    static let kAnonymousCallHandlePrefix = "Sher:"
    let callController = CXCallController()
    let showNamesOnCallScreen: Bool

    
    required init(showNamesOnCallScreen: Bool) {
        AssertIsOnMainThread()
        
        self.showNamesOnCallScreen = showNamesOnCallScreen
        super.init()
        
        // We cannot assert singleton here, because this class gets rebuilt when the user changes relevant call settings
    }
    
    // MARK: Actions
    
    func startCall(_ roomEntity: DSRoomEntity) {
        let handle: CXHandle
        let call = roomEntity.call!
        
        if showNamesOnCallScreen {
            handle = CXHandle(type: .generic, value: roomEntity.displayNameForGroupCall)
        } else {
            let callKitId = DSCallKitCallManager.kAnonymousCallHandlePrefix + call.localId.uuidString
            handle = CXHandle(type: .generic, value: callKitId)
//            CallKitIdStore.setAddress(call.remoteAddress, forCallKitId: callKitId)
        }
        
        let startCallAction = CXStartCallAction(call: call.localId, handle: handle)
        
        startCallAction.isVideo = call.hasLocalVideo
        
        let transaction = CXTransaction()
        transaction.addAction(startCallAction)
        
        requestTransaction(transaction)
    }
    
    func localHangup(call: CallModel) {
        let endCallAction = CXEndCallAction(call: call.localId)
        let transaction = CXTransaction()
        transaction.addAction(endCallAction)
        
        requestTransaction(transaction)
    }
    
    func setHeld(call: CallModel, onHold: Bool) {
        let setHeldCallAction = CXSetHeldCallAction(call: call.localId, onHold: onHold)
        let transaction = CXTransaction()
        transaction.addAction(setHeldCallAction)
        
        requestTransaction(transaction)
    }
    
    func setIsMuted(call: CallModel, isMuted: Bool) {
        let muteCallAction = CXSetMutedCallAction(call: call.localId, muted: isMuted)
        let transaction = CXTransaction()
        transaction.addAction(muteCallAction)
        
        requestTransaction(transaction)
    }
    
    func answer(call: CallModel) {
        let answerCallAction = CXAnswerCallAction(call: call.localId)
        let transaction = CXTransaction()
        transaction.addAction(answerCallAction)
        
        requestTransaction(transaction)
    }
    
    private func requestTransaction(_ transaction: CXTransaction) {
        callController.request(transaction) { error in
            if let error = error {
                Logger.error("Error requesting transaction: \(error)")
            } else {
                Logger.debug("Requested transaction successfully")
            }
        }
    }
    
    // MARK: Call Management
    
    private(set) var rooms = [DSRoomEntity]()
    
    func roomWithLocalId(_ localId: UUID) -> DSRoomEntity? {
        guard let index = rooms.firstIndex(where: { $0.call.localId == localId }) else {
            return nil
        }
        return rooms[index]
    }
    
    func addRoom(_ room: DSRoomEntity) {
        rooms.append(room)
    }
    
    func removeRoom(_ room: DSRoomEntity) {
        rooms.removeFirst(where: { $0 === room })
    }
    
    func removeAllRooms() {
        rooms.removeAll()
    }
}

fileprivate extension Array {
    
    mutating func removeFirst(where predicate: (Element) throws -> Bool) rethrows {
        guard let index = try firstIndex(where: predicate) else {
            return
        }
        
        remove(at: index)
    }
}
