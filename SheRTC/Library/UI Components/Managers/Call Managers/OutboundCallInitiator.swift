//
//  Copyright (c) 2020 Open Whisper Systems. All rights reserved.
//

import Foundation

/**
 * Creates an outbound call via WebRTC.
 */
@objc public class OutboundCallInitiator: NSObject {

    @objc public override init() {
        super.init()

        SwiftSingletons.register(self)
    }

    
    /**
     * | roomEntity is DSRoomEntity
     */
    @discardableResult
    @objc
    public func initiateGroupCall(roomEntity: DSRoomEntity,vc: UIViewController) -> Bool {
        Logger.info("with room: \(roomEntity.localizedDescription())")
        
        return initiateGroupCallWithConfirmation(roomEntity: roomEntity,vc: vc)
    }
    
    /**
     * |roomEntity is DSRoomEntity
     */
    @discardableResult
    @objc
    public func initiateGroupCallWithConfirmation(roomEntity: DSRoomEntity,vc: UIViewController) -> Bool {
        guard let callUIAdapter = AppEnvironment.shared.conferenceCallService.callUIAdapter else {
            owsFailDebug("missing group callUIAdapter")
            return false
        }
//        guard let frontmostViewController = UIApplication.shared.frontmostViewController else {
//            owsFailDebug("could not identify frontmostViewController")
//            return false
//        }
    
        vc.ows_askForMicrophonePermissions { granted in
            guard granted == true else {
                Logger.warn("aborting due to missing microphone permissions.")
//                OWSAlerts.showNoMicrophonePermissionAlert()
                return
            }
            
            callUIAdapter.startAndShowOutgoingCall(roomEntity: roomEntity)
        }
        
        return true
    }
}
