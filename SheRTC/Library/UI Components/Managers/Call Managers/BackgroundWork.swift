//
//  BackgroundWork.swift
//  CometChatSwift
//
//  Created by Max on 01.05.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation
import CometChatPro

public class BackgroundWork: NSObject, CometChatMessageDelegate {
    
    @objc public override init() {
        super.init()
        setupDelegate()
        SwiftSingletons.shared.register(self)
    }
    
    private func setupDelegate() {
         CometChat.messagedelegate = self
    }
    
    public func onTextMessageReceived(textMessage: TextMessage) {
        guard textMessage.muid == kSenderSystemMuid else { return }
        DispatchQueue.main.async {
            if textMessage.text.contains(DSCallCommands.startCall.rawValue) {
                let initiateCallNotifyName = Notification.Name(Notification_IncomingCall)
                NotificationCenter.default.post(name: initiateCallNotifyName, object: textMessage.text, userInfo: nil)
                return
            }
            if textMessage.text.contains(DSCallCommands.busyCall.rawValue) {
                let initiateCallNotifyName = Notification.Name(Notification_RecipientBusy)
                NotificationCenter.default.post(name: initiateCallNotifyName, object: textMessage.text, userInfo: nil)
                return
            }
            if textMessage.text.contains(DSCallCommands.unlinkCall.rawValue) {
                let initiateCallNotifyName = Notification.Name(Notification_UnlinkVideoconferencing)
                NotificationCenter.default.post(name: initiateCallNotifyName, object: textMessage.text, userInfo: nil)
                return
            }
        }
        
    }
}
