//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation
import CometChatPro

enum DSCallCommands: String {
    case startCall = "123123startCall123123:"
    case busyCall = "123123busyCall123123:"
    case unlinkCall = "123123unlinkCall123123:"
}
public let kSenderSystemUID = "SystemCalls"
public let kSenderSystemMuid = "1"

public class DSCallCommandsManager: NSObject {
    private var callUIAdapter: DSCallUIAdapter {
        return AppEnvironment.shared.conferenceCallService.callUIAdapter
    }
    
    @objc
    func initiateCall(_ notification: String,isActiveCall: Bool, completition: @escaping ((DSRoomEntity?) -> Void)) {
        guard let roomEntity = DSRoomEntity.getRoomEntityFromNotification(notification) else { return }
        
        roomEntity.call = CallModel.incomingCall(localId: UUID(),
//                                                  remoteAddress: Group(),
                                                  sentAtTimestamp: UInt64(Date().timeIntervalSince1970))
        roomEntity.call.state = .localRinging
        
        roomEntity.localNumber = CometChat.getLoggedInUser()?.uid ?? ""
        
        if isActiveCall == true {
            sendBusyCommand(roomEntity)
            Logger.error("found active call when trying to report about new one")
            return
        }
        
        DSCallCommandsManager.roomExist(withEntity: roomEntity) { (isExists,isError) in
            guard !isError else {
                Logger.error("Room exist request failed")
                return
            }
            if isExists {
                self.callUIAdapter.reportIncomingCall(roomEntity)
                completition(roomEntity)
            } else {
                completition(nil)
                Logger.info("Call from inactive room with name: \(roomEntity.nameRoom)")
            }
        }
    }
    
    public func unlinkCommand(_ roomEntity: DSRoomEntity?,_ notification: Notification) {
        guard
            let roomId = DSCallCommandsManager.getDataFromNotification(notification,
                                                                         withCommand: DSCallCommands.unlinkCall.rawValue) else {
                                                                            Logger.info("Cannot find room to unlink")
                                                                            return
        }
        
        let roomId64 = Int64(roomId)
        guard roomEntity?.idRoom == roomId64 else { return }
        callUIAdapter.remoteDidHangupCall(roomEntity!)
        Logger.debug("Unlink command received")
    }
    
    public func sendBusyCommand(_ roomEntity: DSRoomEntity) {
        let admin = roomEntity.admin
        
        let localUid = CometChat.getLoggedInUser()?.uid ?? "user"
        let body = DSCallCommands.busyCall.rawValue + localUid
        sendCommand(body: body, user: admin)
    }
    
    public func sendCallCommand(_ roomEntity: DSRoomEntity) {
        guard let body = roomEntity.bodyToSendRoomAsCommand() else {
            Logger.error("Cannot serialize room entity in string")
            return
        }
        let bodyWithCommand = DSCallCommands.startCall.rawValue + body
        sendCommand(body: bodyWithCommand, to: roomEntity.group)
    }
    
    public func sendUnlinkCommand(_ roomEntity: DSRoomEntity) {
        let idRoom = String(roomEntity.idRoom)
        let bodyWithCommand = DSCallCommands.unlinkCall.rawValue + idRoom
        sendCommand(body: bodyWithCommand, to: roomEntity.group)
    }
    
    private func sendCommand(body: String,to groupGuid:String) {
        let textMessage = TextMessage(receiverUid: groupGuid, text: body, receiverType: .group)
        textMessage.muid = kSenderSystemMuid
        textMessage.sender?.uid = kSenderSystemUID
        textMessage.senderUid = kSenderSystemUID
        CometChat.sendTextMessage(message: textMessage, onSuccess: { (message) in
            let metadata : [String : Any]? = textMessage.metaData
            print("sendTextMessage onSuccess: \(String(describing: metadata))")
        }) { (error) in
            print("sendTextMessage error: \(String(describing: error?.errorDescription))")
        }
    }
    
    private func sendCommand(body: String,user:String) {
        let textMessage = TextMessage(receiverUid: user, text: body, receiverType: .user)
        textMessage.muid = kSenderSystemMuid
        textMessage.sender?.uid = kSenderSystemUID
        textMessage.senderUid = kSenderSystemUID
        CometChat.sendTextMessage(message: textMessage, onSuccess: { (message) in
            let metadata : [String : Any]? = textMessage.metaData
            print("sendTextMessage onSuccess: \(String(describing: metadata))")
        }) { (error) in
            print("sendTextMessage error: \(String(describing: error?.errorDescription))")
        }
    }
    
    static func getDataFromNotification(_ notification: Notification,withCommand: String) -> String? {
        let body = notification.object as! String
        guard let index = body.range(of: withCommand)?.upperBound else { return nil }
        let data = String(body[index...])
        return data
    }
}

// MARK: Network request
extension DSCallCommandsManager {
    @objc
    static func roomExist(withEntity roomEntity: DSRoomEntity,completition: @escaping ((_ exist: Bool,_ error: Bool) -> Void)) {
//        let request = OWSRequestFactory.getRoomExist(roomEntity.nameRoom, phoneNumber: "+380933406274", idRoom: String(roomEntity.idRoom))
//        SSKEnvironment.shared.networkManager.makeHTTPRequest(request, success: { (task, response) in
//            guard Data("true".utf8) == response as? Data else {
//                completition(false,false)
//                return
//            }
//            completition(true,false)
//        }) { (task,error ) in
//            completition(false,true)
//            Logger.error("Cant check if room exists \(error.localizedDescription)")
//        }
        completition(true,false)
    }
}
