//
//  NetworkRequests.swift
//  CometChatSwift
//
//  Created by Max on 25.04.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation
import PromiseKit

public class NetworkRequests: NSObject {
    
    public override init() {
        super.init()
        SwiftSingletons.register(self)
    }
    
    let imgurClientId = "b249891d638f690"
    func createNewUser(name: String) -> Promise<UserModel> {
        let uid = name + String(Date().timeIntervalSince1970 * 1000)
        let urlString = "https://api-eu.cometchat.io/v2.0/users"
        let url = URL(string: urlString)!
        
        let parameters = [
            "uid": "\(uid)",
            "name": "\(name)",
            "withAuthToken": "true"
        ]
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        request.httpBody = httpBody
        
        return firstly {
            URLSession.shared.dataTask(.promise, with: request)
        }.compactMap {
            return try JSONDecoder().decode(UserModel.self, from: $0.data)
        }
    }
    
   private func getBase64Image(image: UIImage) -> Promise<String> {
        return Promise { (resolver) in
            let imageData = image.pngData()
            let base64Image = imageData?.base64EncodedString(options: .lineLength64Characters)
            if base64Image == nil {
                let error = NSError(domain: "Cannot build data from image", code: 0, userInfo: nil)
                resolver.reject(error)
            } else {
                resolver.fulfill(base64Image!)
            }
        }
    }
    
   private func uploadToImgur(image: String) -> Promise<String> {
        return Promise { (seal) in
            let boundary = "Boundary-\(UUID().uuidString)"
            
            var request = URLRequest(url: URL(string: "https://api.imgur.com/3/image")!)
            request.addValue("Client-ID \(self.imgurClientId)", forHTTPHeaderField: "Authorization")
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            request.httpMethod = "POST"
            
            var body = ""
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"image\""
            body += "\r\n\r\n\(image)\r\n"
            body += "--\(boundary)--\r\n"
            let postData = body.data(using: .utf8)
            
            request.httpBody = postData
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    return seal.reject(error)
                }
                guard let response = response as? HTTPURLResponse,
                    (200...299).contains(response.statusCode) else {
                        let nsError = NSError(domain: "Wrong response code", code: 300, userInfo: nil)
                        return seal.reject(nsError)
                }
                if let mimeType = response.mimeType, mimeType == "application/json", let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("imgur upload results: \(dataString)")
                    
                    let parsedResult: [String: AnyObject]
                    do {
                        parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: AnyObject]
                        if let dataJson = parsedResult["data"] as? [String: Any] {
                            guard let link = dataJson["link"] as? String else {
                                let error = NSError(domain: "Cant parse link", code: 0, userInfo: nil)
                                return seal.reject(error)
                            }
                            return seal.fulfill(link)
                        }
                    } catch {
                        let error = NSError(domain: "Unexpected error", code: 0, userInfo: nil)
                        return seal.reject(error)
                    }
                }
            }.resume()
        }
    }

    func returnAvatarLink(image: UIImage) -> Promise<String> {
        return Promise { (seal) in
            firstly { () -> Promise<String> in
                return getBase64Image(image: image)
            }.then { [weak self]  (baseImage: String) -> Promise<String> in
                guard let self = self else { throw NSError(domain: "Cancelled", code: 0, userInfo: nil) }
                return self.uploadToImgur(image: baseImage)
            }.done { (link) in
                seal.fulfill(link)
            }.catch { (error) in
                seal.reject(error)
            }
        }
    }
    
    // MARK: Verify Request
    
   private func createVerifyRequest(_ path: String,_ parameters: [String: String]) -> Promise<Data> {
        let file = Bundle.main.path(forResource: "Config", ofType: "plist")!
        let config = NSDictionary(contentsOfFile: file)
        let baseURLString = config!["serverUrl"] as! String
        
        let urlPath = "\(baseURLString)\(path)"
        var components = URLComponents(string: urlPath)!
        
        var queryItems = [URLQueryItem]()
        
        parameters.forEach {
            let item = URLQueryItem(name: $0, value: $1)
            queryItems.append(item)
        }
        
        components.queryItems = queryItems
        
        let url = components.url!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let session: URLSession = {
            let config = URLSessionConfiguration.default
            return URLSession(configuration: config)
        }()
        
        return Promise { (resolver) in
            session.dataTask(with: request) { (data,response,error) -> Void in
                if let jsonData = data {
                    resolver.fulfill(jsonData)
                } else {
                    resolver.reject(VerifyError.err("Response data is nil"))
                }
            }.resume()
        }
    }
    
    func sendVerificationCode(_ countryCode: String,
                              _ phoneNumber: String) -> Promise<DataResult> {
        let parameters = [
            "via" : "sms",
            "country_code" : countryCode,
            "phone_number" : phoneNumber,
        ]
        
        return Promise { (seal) in
            firstly { () -> Promise<Data> in
                return createVerifyRequest("start", parameters)
            } .done { (data) in
//                guard let result = DataResult(data: data) else {
//                    seal.reject(VerifyError.err("Decodable is nil"))
//                }
                let result = DataResult(data: data)
                seal.fulfill(result)
            } .catch { (error) in
                seal.reject(error)
            }
        }
    }
    
    func validateVerificationCode(_ countryCode: String,
                                  _ phoneNumber: String,
                                  verificationCode: String) -> Promise<CheckResult> {
        let parameters = [
            "via" : "sms",
            "country_code" : countryCode,
            "phone_number" : phoneNumber,
            "verification_code" : verificationCode,
        ]
        return Promise { (seal) in
            firstly { () -> Promise<Data> in
               return createVerifyRequest("check", parameters)
            }.done { (data) in
                let check = try JSONDecoder().decode(CheckResult.self, from: data)
                seal.fulfill(check)
            }.catch { (error) in
                seal.reject(error)
            }
        }
    }
}
