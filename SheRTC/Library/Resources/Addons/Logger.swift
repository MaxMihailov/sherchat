//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation

@inlinable
public func owsFormatLogMessage(_ logString: String,
                                file: String = #file,
                                function: String = #function,
                                line: Int = #line) -> String {
    let filename = (file as NSString).lastPathComponent
    // We format the filename & line number in a format compatible
    // with XCode's "Open Quickly..." feature.
    return "[\(filename):\(line) \(function)]: \(logString)"
}

/**
 * A minimal DDLog wrapper for swift.
 */
open class Logger: NSObject {

    open class func verbose(_ logString: @autoclosure () -> String,
                            file: String = #file,
                            function: String = #function,
                            line: Int = #line) {
        guard ShouldLogVerbose() else {
            return
        }
        OWSLogger.verbose(owsFormatLogMessage(logString(), file: file, function: function, line: line))
    }

    open class func debug(_ logString: @autoclosure () -> String,
                          file: String = #file,
                          function: String = #function,
                          line: Int = #line) {
        guard ShouldLogDebug() else {
            return
        }
        OWSLogger.debug(owsFormatLogMessage(logString(), file: file, function: function, line: line))
    }

    open class func info(_ logString: @autoclosure () -> String,
                         file: String = #file,
                         function: String = #function,
                         line: Int = #line) {
        guard ShouldLogInfo() else {
            return
        }
        OWSLogger.info(owsFormatLogMessage(logString(), file: file, function: function, line: line))
    }

    open class func warn(_ logString: @autoclosure () -> String,
                         file: String = #file,
                         function: String = #function,
                         line: Int = #line) {
        guard ShouldLogWarning() else {
            return
        }
        OWSLogger.warn(owsFormatLogMessage(logString(), file: file, function: function, line: line))
    }

    open class func error(_ logString: @autoclosure () -> String,
                          file: String = #file,
                          function: String = #function,
                          line: Int = #line) {
        guard ShouldLogError() else {
            return
        }
        OWSLogger.error(owsFormatLogMessage(logString(), file: file, function: function, line: line))
    }

    open class func flush() {
        OWSLogger.flush()
    }
}


public func assertOnQueue(_ queue: DispatchQueue) {
    if #available(iOS 10.0, *) {
        dispatchPrecondition(condition: .onQueue(queue))
    } else {
        // Skipping check on <iOS10, since syntax is different and it's just a development convenience.
    }
}

@inlinable
public func AssertIsOnMainThread(file: String = #file,
                                 function: String = #function,
                                 line: Int = #line) {
    if !Thread.isMainThread {
        owsFailDebug("Must be on main thread.", file: file, function: function, line: line)
    }
}

@inlinable
public func owsFailDebug(_ logMessage: String,
                         file: String = #file,
                         function: String = #function,
                         line: Int = #line) {
    Logger.error(logMessage, file: file, function: function, line: line)
    Logger.flush()
    let formattedMessage = owsFormatLogMessage(logMessage, file: file, function: function, line: line)
//    assertionFailure(formattedMessage)
}

@inlinable
public func owsFail(_ logMessage: String,
                    file: String = #file,
                    function: String = #function,
                    line: Int = #line) -> Never {
    OWSSwiftUtils.logStackTrace()
    owsFailDebug(logMessage, file: file, function: function, line: line)
    let formattedMessage = owsFormatLogMessage(logMessage, file: file, function: function, line: line)
    fatalError(formattedMessage)
}

@inlinable
public func notImplemented(file: String = #file,
                           function: String = #function,
                           line: Int = #line) -> Never {
    owsFail("Method not implemented.", file: file, function: function, line: line)
}

@objc
public class OWSSwiftUtils: NSObject {
    // This method can be invoked from Obj-C to exit the app.
    @objc
    public class func owsFail(_ logMessage: String,
                              file: String = #file,
                              function: String = #function,
                              line: Int = #line) -> Never {

        logStackTrace()
        owsFailDebug(logMessage, file: file, function: function, line: line)
        let formattedMessage = owsFormatLogMessage(logMessage, file: file, function: function, line: line)
        fatalError(formattedMessage)
    }

    @objc
    public class func logStackTrace() {
        Thread.callStackSymbols.forEach { Logger.error($0) }
    }
}

