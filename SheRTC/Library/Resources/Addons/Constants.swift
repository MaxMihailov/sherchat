//
//  Constants.swift
//  CometChatSwift
//
//  Created by Max on 09.04.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation

let kMessageShowVideo = "show_my_video"
let kMessageHideVideo = "hide_my_video"
let kMessageMicIsOn = "my_mic_is_on"
let kMessageMicIsOff = "my_mic_is_off"
let kMessageAllMuted = "all_muted_array"
let kMessageAllNoVideo = "all_novideo_array"
let kMessageUpdateLocalMedia  = "update_local_media"

let Notification_IncomingCall = "NSNotificationName_DStarCommandType_IncomingConferenceCall"
let  Notification_RecipientBusy = "NSNotificationName_DStarCommandType_RecipientBusyCallCommand"
let  Notification_UnlinkVideoconferencing = "NSNotificationName_DStarCommandType_UnlinkVideoconferencing"

