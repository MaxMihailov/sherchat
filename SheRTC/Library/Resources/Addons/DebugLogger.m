//
//  Copyright (c) 2019 Open Whisper Systems. All rights reserved.
//

#import "DebugLogger.h"

#pragma mark Logging - Production logging wants us to write some logs to a file in case we need it for debugging.
#import <CocoaLumberjack/DDTTYLogger.h>

NS_ASSUME_NONNULL_BEGIN

const NSUInteger kMaxDebugLogFileSize = 1024 * 1024 * 3;

@interface DebugLogger ()

@property (nonatomic, nullable) DDFileLogger *fileLogger;

@end

#pragma mark -

@implementation DebugLogger

+ (instancetype)sharedLogger
{
    static DebugLogger *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [self new];
    });
    return sharedManager;
}


- (void)enableTTYLogging
{
    [DDLog addLogger:DDTTYLogger.sharedInstance];
}

- (id<DDLogger>)errorLogger
{
    static id<DDLogger> instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        id<DDLogFileManager> logFileManager =
            [[DDLogFileManagerDefault alloc] initWithLogsDirectory:self.errorLogsDir.path
                                        defaultFileProtectionLevel:@""];

        instance = [[ErrorLogger alloc] initWithLogFileManager:logFileManager];
    });

    return instance;
}

- (void)enableErrorReporting
{
    [DDLog addLogger:self.errorLogger withLevel:DDLogLevelOff];
}

@end

@implementation ErrorLogger

- (void)logMessage:(nonnull DDLogMessage *)logMessage
{
    [super logMessage:logMessage];
}


@end

NS_ASSUME_NONNULL_END
