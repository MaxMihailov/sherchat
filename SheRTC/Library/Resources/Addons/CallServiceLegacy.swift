//
//  CallServiceLegacy.swift
//  CometChatSwift
//
//  Created by Max on 10.04.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation
// All Observer methods will be invoked from the main thread.
protocol CallServiceObserver: class {
    /**
     * Fired whenever the call changes.
     */
    func didUpdateCall(call: CallModel?)

    /**
     * Fired whenever the local or remote video track become active or inactive.
     */
    func didUpdateVideoTracks(call: CallModel?,
                              localCaptureSession: AVCaptureSession?,
                              remoteVideoTrack: RTCVideoTrack?)
}
