//
//  Copyright (c) 2019 Open Whisper Systems. All rights reserved.
//

import Foundation

@objc public extension UIApplication {
    
//    var frontmostViewControllerIgnoringAlerts: UIViewController? {
//        return findFrontmostViewController(ignoringAlerts: true, window: window)
//    }
//    
//    var frontmostViewController: UIViewController? {
//        return findFrontmostViewController(ignoringAlerts: false, window: window)
//    }
//    
//    func findFrontmostViewController(ignoringAlerts: Bool, window: UIWindow) -> UIViewController? {
//        Logger.verbose("findFrontmostViewController: \(window)")
//        guard let viewController = window.rootViewController else {
//            owsFailDebug("Missing root view controller.")
//            return nil
//        }
//        return viewController.findFrontmostViewController(ignoringAlerts)
//    }
//
//    func openSystemSettings() {
//        openURL(URL(string: UIApplication.openSettingsURLString)!)
//    }

}

extension UIWindow {

    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }

    static func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        if let navigationController = vc as? UINavigationController,
            let visibleController = navigationController.visibleViewController  {
            return UIWindow.getVisibleViewControllerFrom( vc: visibleController )
        } else if let tabBarController = vc as? UITabBarController,
            let selectedTabController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: selectedTabController )
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
}
