//
//  VerificationModel.swift
//  CometChatSwift
//
//  Created by Max on 19.05.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation

enum VerifyError: Error {
    case invalidUrl
    case err(String)
}

protocol WithMessage {
    var message: String { get }
}

enum VerifyResult {
    case success(WithMessage)
    case failure(Error)
}

class DataResult: WithMessage {
    let data: Data
    let message: String
    
    init(data: Data) {
        self.data = data
        self.message = String(describing: data)
    }
}

struct CheckResult: Codable, WithMessage {
    let success: Bool
    let message: String
}


