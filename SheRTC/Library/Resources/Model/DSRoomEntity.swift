//
//  Copyright (c) 2018 Open Whisper Systems. All rights reserved.
//

import Foundation
import CometChatPro

enum TypeOfRoom: String,Codable{
    case PTT
    case VIDEO_CHAT
    case VIDEO_CONFERENCE
}

@objcMembers
public class DSRoomEntity: NSObject, Codable {
    var displayNameForGroupCall: String
    var nameRoom: String
    var idRoom: Int64
    var listParticipants: [String]
    var listParticipantsForLeave: [String]?
    var roomIdsForDelete: [String]?
    var typeOfRoom: TypeOfRoom
    var admin: String
    var group: String!
    // Local room entity var
    var localNumber: String!
    var call: CallModel!
    var noAudioList = Set<String>()
    var noVideoList = Set<String>()
    var isFirstCaller: Bool = false
//    {
//        willSet {
//            if newValue {
//                self.setAdmin(number:CometChat.getLoggedInUser()?.uid)
//            }
//        }
//    }
    
    private enum CodingKeys: String, CodingKey {
        case displayNameForGroupCall
        case nameRoom
        case idRoom
        case admin
        case group
        case listParticipants
        case listParticipantsForLeave
        case roomIdsForDelete
        case typeOfRoom
    }
    
    /// Init
    ///
    /// - Parameters:
    ///   - roomName: room id
    ///   - displayName: name of group to display
    ///   - recipients: group call members
    ///   - incoming: false if outgoung
    @objc
    init(roomName:String,displayName: String,incoming: Bool,participants: [String],group: String) {
        let id = Int.random(in: -999..<0)
        self.idRoom = Int64(id)
        self.nameRoom = roomName
        self.displayNameForGroupCall = displayName
        self.typeOfRoom = TypeOfRoom.VIDEO_CHAT
        self.group = group
        self.admin = CometChat.getLoggedInUser()?.uid  ?? "admin"
        self.listParticipants = participants
        self.localNumber = CometChat.getLoggedInUser()?.uid
        
        if incoming {
            self.call = CallModel.incomingCall(localId: UUID(), sentAtTimestamp: UInt64(Date().timeIntervalSince1970))
        } else {
            self.call = CallModel.outgoingCall(localId: UUID())
        }
    }
    
//    public func setAdmin(number: String?) {
//        guard number != nil, listParticipants[number!] != nil else { return }
//        listParticipants[number!] = true
//    }
    
//    public func isValid() -> Bool {
//        guard listParticipants.count > 0 &&  nameRoom.count > 0 else {
//            return false
//        }
//        return true
//    }
}
// MARK: - Static functions
extension DSRoomEntity {
//    static func getRoomParticipantsFromNSArray(recipients: NSArray) -> [String: Bool] {
//        var listParticipants = [String:Bool]()
//        let recipients = recipients.compactMap({ $0 as? SignalServiceAddress })
//        let numbers = recipients.compactMap({ $0.phoneNumber })
//        numbers.forEach({ listParticipants[$0] = false })
//        return listParticipants
//    }
    
    static func getRoomEntityFromNotification(_ notification: String) -> DSRoomEntity? {
        let data = notification.data(using: .utf8)!
        let roomEntity = try? JSONDecoder().decode(DSRoomEntity.self, from: data)
        return roomEntity
    }
    
    static func getRoomEntityForGroup(_ groupThread: Group,participants:[String]) -> DSRoomEntity {
//        let allRecipients = recipients.adding(TSAccountManager.localAddress!) as NSArray
//        let prefix = "__textsecure_group__!";
        let displayName = groupThread.name ?? "room"
//        let roomName = prefix + displayName // TODO: Use this when develop is ready
        let roomName = displayName
        let room = DSRoomEntity(roomName: roomName, displayName: displayName, incoming: false,participants: participants,group: groupThread.guid)
        room.call.state = .idle
        return room
    }
    
    static func getRoomEntityForHugeGroup(_ groupThread: Group,participants:[String]) -> DSRoomEntity {
        let room = getRoomEntityForGroup(groupThread,participants: participants)
        room.nameRoom = room.nameRoom + String(Date().timeIntervalSince1970)
        room.isFirstCaller = true
        return room
    }

}
// MARK: - Helpers
extension DSRoomEntity {
//    func getAdmin() -> String? {
//        return listParticipants.first(where: {$0.value == true})?.key
//    }
//
//    func getParticipantsArray() -> [String] {
//        return [String](listParticipants.keys)
//    }
    
//    func getSignalServiceAddressArray() -> [SignalServiceAddress] {
//        return getParticipantsArray().map({ SignalServiceAddress(phoneNumber: $0) })
//    }
    
    //Check behaviour of this method
//    func getFirstAddress() -> SignalServiceAddress {
//        return getSignalServiceAddressArray().first!
//    }
    
//    func addToParticipantsIfNeeded(number: String) {
//        guard listParticipants[number] == nil else { return }
//        listParticipants[number] = false
//    }
    
    func saveAudioVideoStates(for number: String,isVideo: Bool,active: Bool) {
        if isVideo {
            if active {
                noVideoList.remove(number)
            } else {
                noVideoList.insert(number)
            }
        } else {
            if active {
                noAudioList.remove(number)
            } else {
                noAudioList.insert(number)
            }
        }
    }
    
    func removeAudioVideoStates(for number: String) {
        noVideoList.remove(number)
        noAudioList.remove(number)
    }
    
    func bodyToSendRoomAsCommand() -> String? {
        let encodedRoomEntity: Data
        do {
            encodedRoomEntity = try JSONEncoder().encode(self)
        } catch let error as NSError {
            Logger.error("Cant encode room entity \(error.localizedDescription)")
            return nil
        }
        let stringRoomEntity = String(decoding: encodedRoomEntity, as: UTF8.self)
        return stringRoomEntity
    }

    func localizedDescription() -> String {
        return "Name: \(self.nameRoom), Type: \(typeOfRoom)"
    }
}
