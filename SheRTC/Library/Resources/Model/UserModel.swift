//
//  UserModel.swift
//  CometChatSwift
//
//  Created by Max on 25.04.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation

class UserModel: Decodable {
    var data: UserModelData
}

class UserModelData: Decodable {
    var uid: String
    var name: String
    var status: String
    var role: String
    var createdAt: Int
    var authToken: String
}
