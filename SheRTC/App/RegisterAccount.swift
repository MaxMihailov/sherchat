//
//  RegisterAccount.swift
//  CometChatSwift
//
//  Created by Max on 25.04.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation
import CometChatPro
import PromiseKit

class RegisterAccount: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPassField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    
    // Dependecies
    
    var networkRequests: NetworkRequests {
        return AppEnvironment.shared.networkRequests
    }
    
    override func viewDidLoad() {
        self.hideKeyboardWhenTappedArround()
    }
    
    fileprivate func hideKeyboardWhenTappedArround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    // This function dismiss the  keyboard
    @objc  func dismissKeyboard() {
        loginField.resignFirstResponder()
        passwordField.resignFirstResponder()
        confirmPassField.resignFirstResponder()
    }
    
    @IBAction func registerDidPress(_ sender: UIButton) {
        guard let login = loginField.text, login.count > 0 else {
                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_LOGIN_TOO_SMALL", comment: ""), duration: .middle)
                snackbar.show()
                return
            }
            guard let countryCode = passwordField.text, countryCode.count > 0 else {
                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_TOO_SMALL", comment: ""), duration: .middle)
                snackbar.show()
                return
            }
            guard let phoneNumber = confirmPassField.text, phoneNumber.count > 0 else {
                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_CONFIRM_TOO_SMALL", comment: ""), duration: .middle)
                snackbar.show()
                return
            }
//            guard passConfirm == pass else {
//                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_NOT_EQUAL", comment: ""), duration: .middle)
//                snackbar.show()
//                return
//            }
            guard !login.hasSpecialCharacters() else {
                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_LOGIN_NOT_VALID", comment: ""), duration: .middle)
                snackbar.show()
                return
        }
        
        firstly { () -> Promise<DataResult> in
            return  networkRequests.sendVerificationCode(countryCode, phoneNumber)
        } .done { (dataResult) in
            Logger.debug("Done\(dataResult.message)")
        } .catch { (error) in
            Logger.error(error.localizedDescription)
        }
       
        
        //            guard !pass.hasSpecialCharacters() else {
        //                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_NOT_VALID", comment: ""), duration: .middle)
//                snackbar.show()
//                return
//            }
            
        
        
//        guard let login = loginField.text, login.count > 0 else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_LOGIN_TOO_SMALL", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//        guard let pass = passwordField.text, pass.count > 6 else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_TOO_SMALL", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//        guard let passConfirm = confirmPassField.text, passConfirm.count > 6 else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_CONFIRM_TOO_SMALL", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//        guard passConfirm == pass else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_NOT_EQUAL", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//        guard !login.hasSpecialCharacters() else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_LOGIN_NOT_VALID", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//        guard !pass.hasSpecialCharacters() else {
//            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_PASS_NOT_VALID", comment: ""), duration: .middle)
//            snackbar.show()
//            return
//        }
//
//        let user = User(uid: login, name: "New User")
//        if nameField.text?.count ?? 0 > 0 {
//            user.name = nameField.text!
//        }
//
//        CometChat.createUser(user: user, apiKey: Constants.apiKey, onSuccess: { (user) in
//            CometChat.login(UID: user.uid!, apiKey: Constants.apiKey, onSuccess: { (user) in
//                DispatchQueue.main.async {
//                    let unified =  CometChatUnified()
//                    unified.setup(withStyle: .fullScreen)
//                    self.present(unified, animated: true, completion: nil)
//                }
//            }) { (error1) in
//                Logger.error("Error while trying to login")
//            }
//        }) { (error2) in
//            Logger.error("Erorr while trying to register")
//        }
//    }
       
    }
    
 
}

extension String {
    func hasSpecialCharacters() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: ".*[0-9].*", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)) {
                return true
            }

        } catch {
            debugPrint(error.localizedDescription)
            return false
        }

        return false
    }
}
