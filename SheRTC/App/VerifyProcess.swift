//
//  VerifyProcess.swift
//  CometChatSwift
//
//  Created by Max on 19.05.2020.
//  Copyright © 2020 MacMini-03. All rights reserved.
//

import Foundation
import PromiseKit
import CometChatPro

class VerifyProcess: UIViewController {
    var codeNumber: String?
    var phoneNumber: String?
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet var doneButton: UIView!
    @IBOutlet weak var doneBottomConstraint: NSLayoutConstraint!
    // Dependencies
    var networkRequests: NetworkRequests {
        return AppEnvironment.shared.networkRequests
    }
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.registerObservers()
    }
    
    fileprivate func registerObservers(){
          //Register Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.hideKeyboardWhenTappedArround()
    }
    
    fileprivate func hideKeyboardWhenTappedArround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        backgroundView.addGestureRecognizer(tap)
    }
    
    // This function dismiss the  keyboard
    @objc  func dismissKeyboard() {
        codeTextField.resignFirstResponder()
//        countryCode.resignFirstResponder()
//
        if self.doneButton.frame.origin.y != 0 {
            doneBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let modelName = UIDevice.modelName
        if let userinfo = notification.userInfo {
            let keyboardHeight = (userinfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue?.size.height
            if (modelName == "iPhone X" || modelName == "iPhone XS" ||
                modelName == "iPhone XR" || modelName == "iPhone12,1") {
                doneBottomConstraint.constant = (keyboardHeight)! + 10
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            } else {
                doneBottomConstraint.constant = (keyboardHeight)! + 5
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        dismissKeyboard()
        
        guard let phoneCode = codeNumber, let phoneNumber = phoneNumber else {
            let loginErrorString = NSLocalizedString("ERROR_OCCURED", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
            
            dismiss(animated: false, completion: nil)
            return
        }
        
        guard let code = codeTextField.text,code.count == 6 else {
            let loginErrorString = NSLocalizedString("LOGIN_CONF", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
            return
        }
        
        self.activityIndicator.startAnimating()
        
        firstly { () -> Promise<CheckResult> in
            return networkRequests.validateVerificationCode(phoneCode, phoneNumber, verificationCode: code)
        } .done { (result) in
            if result.success {
                self.loginWithUID(UID: phoneCode + phoneNumber)
            } else {
                let loginErrorString = NSLocalizedString("CONFIRMATION_WRONG", comment: "")
                let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
                snackbar.show()
            }
        }.catch { (error) in
            let loginErrorString = NSLocalizedString("ERROR_OCCURED", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
        }.finally {
            self.activityIndicator.stopAnimating()
        }
    }
    
    
    private func loginWithUID(UID:String){
        
        activityIndicator.startAnimating()
        if(Constants.apiKey.contains(NSLocalizedString("Enter", comment: "")) || Constants.apiKey.contains(NSLocalizedString("ENTER", comment: "")) || Constants.apiKey.contains("NULL") || Constants.apiKey.contains("null") || Constants.apiKey.count == 0){
            showAlert(title: NSLocalizedString("Warning!", comment: ""), msg: NSLocalizedString("Please fill the APP-ID and API-KEY in Constants.swift file.", comment: ""))
        } else {
            
            CometChat.login(UID: UID, apiKey: Constants.apiKey, onSuccess: { (current_user) in
                
                let userID:String = current_user.uid!
                UserDefaults.standard.set(userID, forKey: "LoggedInUserUID")
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    let unified =  CometChatUnified()
                    unified.setup(withStyle: .fullScreen)
                    self.present(unified, animated: true, completion: nil)
                }
            }) { (error) in
                guard error.errorCode != "ERR_UID_NOT_FOUND" else {
                    self.createCometUserAndEnter(login: UID, name: UID)
                    return
                }
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    DispatchQueue.main.async {
                        let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: error.errorDescription, duration: .short)
                        snackbar.show()
                    }
                }
                print("login failure \(error.errorDescription)")
                
            }
        }
    }
    
    private func createCometUserAndEnter(login: String,name: String) {
         let user = User(uid: login, name: name)
         CometChat.createUser(user: user, apiKey: Constants.apiKey, onSuccess: { (user) in
             CometChat.login(UID: user.uid!, apiKey: Constants.apiKey, onSuccess: { (user) in
                UserDefaults.standard.set(login, forKey: "LoggedInUserUID")
                 DispatchQueue.main.async {
                     let unified =  CometChatUnified()
                     unified.setup(withStyle: .fullScreen)
                     self.present(unified, animated: true, completion: nil)
                 }
             }) { (error1) in
                 Logger.error("Error while trying to login")
             }
         }) { (error2) in
             Logger.error("Erorr while trying to register")
         }
     }
    
}
