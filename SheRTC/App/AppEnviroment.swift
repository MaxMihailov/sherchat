//
//  Copyright (c) 2019 Open Whisper Systems. All rights reserved.
//

import Foundation

@objc public class AppEnvironment: NSObject {

    private static var _shared: AppEnvironment = AppEnvironment()

    @objc
    public class var shared: AppEnvironment {
        get {
            return _shared
        }
        set {

            _shared = newValue
        }
    }

    @objc
    public var outboundCallInitiator: OutboundCallInitiator
    
    @objc
    public var conferenceCallService: DSGroupCallService

    @objc
    public var backgroundWork: BackgroundWork
    
    @objc
    public var networkRequests: NetworkRequests

    private override init() {
        self.conferenceCallService = DSGroupCallService()
        self.outboundCallInitiator = OutboundCallInitiator()
        self.backgroundWork = BackgroundWork()
        self.networkRequests = NetworkRequests()
        super.init()

        SwiftSingletons.register(self)
    }

    @objc
    public func setup() {
        self.conferenceCallService.createUIAdapter()
    }
    
}
