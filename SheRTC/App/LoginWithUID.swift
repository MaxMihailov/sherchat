//
//  ViewController.swift
//  Demo
//
//  Created by CometChat Inc. on 16/12/19.
//  Copyright © 2020 CometChat Inc. All rights reserved.
//

import UIKit
import PromiseKit
import CometChatPro

class LoginWithUID: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var countryCode: UITextField!
    @IBOutlet weak var signInBottomConstraint: NSLayoutConstraint!
    
    let modelName = UIDevice.modelName
    
    // Dependencies
    var networkRequests: NetworkRequests {
        return AppEnvironment.shared.networkRequests
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerObservers()
    }
    
    fileprivate func registerObservers(){
        //Register Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.hideKeyboardWhenTappedArround()
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userinfo = notification.userInfo
        {
            let keyboardHeight = (userinfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue?.size.height
            if (modelName == "iPhone X" || modelName == "iPhone XS" || modelName == "iPhone XR" || modelName == "iPhone12,1"){
                signInBottomConstraint.constant = (keyboardHeight)! + 20
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            } else {
                signInBottomConstraint.constant = (keyboardHeight)! + 5
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    fileprivate func hideKeyboardWhenTappedArround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        backgroundView.addGestureRecognizer(tap)
    }
    
    // This function dismiss the  keyboard
    @objc  func dismissKeyboard() {
        phoneNumber.resignFirstResponder()
        countryCode.resignFirstResponder()
        
        if self.signIn.frame.origin.y != 0 {
            signInBottomConstraint.constant = 20
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        guard let phoneNumber = phoneNumber.text, phoneNumber.count > 0 else {
            let loginErrorString = NSLocalizedString("LOGIN_NUMBER_TOO_SMALL", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
            return
        }
        guard let countryCode = countryCode.text, countryCode.count > 0 else {
            let loginErrorString = NSLocalizedString("LOGIN_NUMBER_TOO_SMALL", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
            return
        }
        
        guard phoneNumber.hasSpecialCharacters() && countryCode.hasSpecialCharacters() else {
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: NSLocalizedString("REGISTER_LOGIN_NOT_VALID", comment: ""), duration: .middle)
            snackbar.show()
            return
        }
        
        self.activityIndicator.startAnimating()
        firstly { () -> Promise<DataResult> in
            return  networkRequests.sendVerificationCode(countryCode, phoneNumber)
        } .done { (dataResult) in
            Logger.debug("Done\(dataResult.message)")
            self.presentVerifyView(code: countryCode,phone: phoneNumber)
        } .catch { (error) in
            let loginErrorString = NSLocalizedString("ERROR_OCCURED", comment: "")
            let snackbar: CometChatSnackbar = CometChatSnackbar.init(message: loginErrorString, duration: .middle)
            snackbar.show()
            Logger.error(error.localizedDescription)
        }.finally {
            self.activityIndicator.stopAnimating()
        }
        
        //        self.loginWithUID(UID: textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
    }
    
    func presentVerifyView(code: String,phone: String) {
        
        let verifyVC = self.storyboard?.instantiateViewController(withIdentifier: "verify") as! VerifyProcess
        verifyVC.codeNumber = code
        verifyVC.phoneNumber = phone
        self.navigationController?.pushViewController(verifyVC, animated: true)
    }
    
   
}

