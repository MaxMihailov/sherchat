//
//  AppDelegate.swift
//  Demo
//
//  Created by CometChat Inc. on 16/12/19.
//  Copyright © 2020 CometChat Inc. All rights reserved.
//

import UIKit
import CometChatPro

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.initialization()
        AppEnvironment.shared.setup()
        AppEnvironment.shared.conferenceCallService.createUIAdapter()
        
        DebugLogger().enableTTYLogging()
        return true
    }
    
    func initialization(){
        if (Constants.appId.contains(NSLocalizedString("Enter", comment: "")) ||
            Constants.appId.contains(NSLocalizedString("ENTER", comment: "")) ||
            Constants.appId.contains("NULL") || Constants.appId.contains("null") ||
            Constants.appId.count == 0) {

        }else{
            let appSettings = AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region: Constants.region).build()
            CometChat.init(appId:Constants.appId, appSettings: appSettings, onSuccess: { (Success) in
                print( "Initialization onSuccess \(Success)")
            }) { (error) in
                print( "Initialization Error \(error.errorDescription)")
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        CometChat.startServices()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        CometChat.startServices()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        CometChat.startServices()// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

